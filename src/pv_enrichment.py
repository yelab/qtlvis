# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 12:11:51 2015
@author: dlituiev
"""
import pandas as pd

chrm = 2
pv = 'results/cisw1e5.a0.r0/qtl_atac_rna.cisw1e5.a0.r0.chr%s.matrix.eqtl.cis.txt' % chrm
dfpv = pd.read_csv(pv, sep = '\t')

"meta rna"
metafile = 'in_data/rna.genes.meta.matrix.eqtl.txt'
metadf = pd.read_csv(metafile, sep = '\t',index_col = 0)

metadf.loc[dfpv['gene']]

"expression"
expr = 'rna.genes.0.matrix.eqtl.txt.matched'
dfexpr = pd.read_csv(expr, index_col = 0, sep = '\t')
medexpr = dfexpr.mean(axis = 1)

"chromatin"
chtn = 'atac.matrix.eqtl.txt.matched'
dfchtn = pd.read_csv(chtn, index_col = 0, sep = '\t')
medchtn = dfchtn.mean(axis = 1)

dfpv['expression quantile'] = pd.np.nan
dfpv['chromatin quantile'] = pd.np.nan

for x in dfpv['SNP']:
    dfpv.loc[x==dfpv['SNP'],'chromatin quantile'] = (medchtn < medchtn[x]).astype(int).mean()

for x in dfpv['gene']:    
    dfpv.loc[x==dfpv['gene'],'expression quantile'] = (medexpr < medexpr[x]).astype(int).mean()

import matplotlib.pyplot as plt
import pdscatter as pdsc

FORMAT = 'pdf'

dfpv['gene label'] = pd.Categorical.from_array(dfpv['gene'].astype('category')).codes

dfpv['log10 (p-value)'] = dfpv['p-value'].apply(pd.np.log10)

fig,_ = pdsc.scatter_df(dfpv, 'gene label','chromatin quantile', 'log10 (p-value)')
#plt.axis('equal')
# plt.xlim((0,1))
plt.ylim((0,1))
plt.grid(True, which='major')
# fig.savefig('results/chrmt_expr_p_val.chr%s.%s' % (chrm, FORMAT), format = FORMAT, dpi=300)


fig,_ = pdsc.scatter_df(dfpv, 'chromatin quantile','expression quantile', 'log10 (p-value)')
plt.axis('equal')
plt.xlim((0,1))
plt.ylim((0,1))
plt.grid(True, which='major')
fig.savefig('results/chrmt_expr_p_val.chr%s.%s' % (chrm, FORMAT), format = FORMAT, dpi=300)

fig,_ = pdsc.scatter_df(dfpv, 
            'log10 (p-value)', 'chromatin quantile','expression quantile',
            clim = (0,1))
plt.ylim((0,1))
plt.savefig('results/p_val_chrmt_expr.chr%s.%s' % (chrm, FORMAT), format = FORMAT, dpi=300)

fig,_ = pdsc.scatter_df(dfpv,
            'log10 (p-value)','expression quantile', 'chromatin quantile',
            clim = (0,1))
plt.ylim((0,1))
plt.savefig('results/p_val_expr_chrmt.chr%s.%s' % (chrm, FORMAT), format = FORMAT, dpi=300)


plt.scatter