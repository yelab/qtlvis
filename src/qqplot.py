# -*- coding: utf-8 -*-
"""
Created on Tue Aug 25 08:47:57 2015

@author: dlituiev
"""
from __future__ import print_function, division
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.stats as stats

def diagonal(ax=None, ls="-", c=[.67]*3, **kwargs):
    if ax is None:
        ax = plt.gca()
    ax.set_xlim(ax.get_ylim())
    diag_line, = ax.plot(ax.get_xlim(), ax.get_ylim(),ls=ls, c=c, zorder=-1, **kwargs )

    def on_change(axes):
        # When this function is called it checks the current
        # values of xlim and ylim and modifies diag_line
        # accordingly.
        x_lims = ax.get_xlim()
        y_lims = ax.get_ylim()
        lims = (0, min(x_lims[1], y_lims[1]) )
        diag_line.set_data(lims, lims)

    # Connect two callbacks to your axis instance.
    # These will call the function "on_change" whenever
    # xlim or ylim is changed.
    ax.callbacks.connect('xlim_changed', on_change)
    ax.callbacks.connect('ylim_changed', on_change)

def qqprep_df(pvector, log_input=False, ntests=None):
    e,o, order = qqprep(pvector, log_input=log_input, ntests=ntests)
    eo = pd.DataFrame(np.c_[e,o], columns = ["expected", "observed"])
    if hasattr(pvector, "index"):
        eo.index = pvector.index[order]
    return eo

def qqprep(pvector, log_input=False, ntests=None):
    """prepare data for QQ-plot from a vector of (neg) (log) p-values"""
    pvector = np.asarray(pvector)
    if not log_input:
        order = np.argsort(pvector)
        o = -np.log10(pvector)[order]
    else:
        order = np.argsort(pvector)[::-1]
        o = pvector[order]
    if ntests is None:
        # see:
        # https://github.com/cran/MatrixEQTL/blob/e74faae75e135f3299a185c985cf4cf644d11169/R/Matrix_eQTL_engine.R#L1836
        ntests = sum(~np.isnan(o))
        #ntests = len(o) 
    e = -np.log10( np.arange(1, 1+len(o))/ ntests )
    return (e, o, order)


def qqplot(pvector, diagonal=True, log_input=False, linecolor=[0.67]*3,
           square = False, ntests=None, verbose=False, results = False, ax=None, **kwargs):
    """theoretical qq-plot for p-values"""
    default_args = {"marker":".", "edgecolor":'none'}
    if "label" in kwargs:
        label = kwargs.pop("label")
    elif hasattr(pvector, "name"):
        label = pvector.name
    default_args.update(kwargs)

    if not ax:
        ax = plt.gca()
    if ax.get_ylim()[1] > 1.0:
        plot_max_observed = ax.get_ylim()[1]
    else:
        plot_max_observed = 0.0

    if "c" not in default_args and "color" not in default_args:
        try:
             default_args["c"] = next(ax._get_lines.prop_cycler)['color']
        except:
            pass

    # calculate expected values
    e, o, order = qqprep(pvector, log_input=log_input, ntests=ntests)
    # calculate axis limits
    max_expected = max(e[~np.isinf(e)])
    max_observed = max(o[~np.isinf(o)])
    plot_max_observed = max(plot_max_observed, max_observed)

    scpl = ax.scatter(e, o, label=label, **default_args)
    if diagonal:
        linelims = [0, max_expected]
        ax.plot(linelims, linelims, c=linecolor, zorder=0)

    if verbose:
        print("ntests", ntests)
        print("max_expected", max_expected)
        print("max_observed", max_observed)

    ax.set_xlim([-0.05, 1.05*max_expected])
    ax.set_ylim([-0.05, 1.05*plot_max_observed])
    #ax.autoscale_view(scalex=False, tight=True)
    ax.set_xlabel("Expected $-\log_{10} p$")
    ax.set_ylabel("Observed $-\log_{10} p$")

    if square:
        ax.axis('equal')
        ax.axis('square')
        ax.set_xticks(ax.get_yticks())
    if results:
        e_ = np.zeros_like(o)
        e_[order] = e
        return ax, e_ #np.vstack([o]).T
    return ax


def qq_highlight(ds, highlight, log_input=True, fontdict=None):
    ax, res = qqplot(ds, log_input=log_input, results=True)
    if ds.name is None:
        ds.name = "observed"
    obs = ds.name
    if log_input:
        ds = pd.DataFrame(ds)
    else:
        ds = pd.DataFrame(ds.map(lambda x: -np.log10(x)))
    ds["expected"] = res

    ax.scatter(ds.loc[highlight, "expected"],
                ds.loc[highlight, obs],
                c="r"
               )
    for hh in highlight:
        ax.text(ds.loc[hh, "expected"], ds.loc[hh, obs].tolist(), hh, 
                horizontalalignment='right', verticalalignment="bottom",
                fontdict=fontdict)
    return ax


def qqplot_emp_(px, py, statname="p-values"):
    class nulldist():
        def __init__(self, datapoints):
            self.datapoints = datapoints
        #t = np.arange(len(px))/(len(px)+1)
        def ppf(self, t):
            return stats.mstats.mquantiles(self.datapoints, t)

    pnull = nulldist(py)
    fig, ax = plt.subplots(1,1, figsize = (3,3))
    stats.probplot(px, dist=pnull, plot=ax)
    xlims = plt.xlim()
    plt.plot(xlims, xlims, color = (0,0,0,.4), zorder = -1)
    plt.axis('equal')
    plt.xlim(xlims)
    plt.ylim(xlims)
    ax.set_xlabel("Nominal %s" % statname)
    ax.set_xlabel("Permutation %s" % statname)


def emp_qq(d1, d0):
    d1.sort()
    length = len(d1) if type(d1) is list else d1.shape[0]
    ecdf = (np.arange(length) - 1/2 )/ len(d1)
    d0out =  stats.mstats.mquantiles(d0, ecdf)
    return (d0out, d1)

def qqplot_emp(d1, d0, ls = 'r.', title = "Empirical Q-Q plot",
                xlab = "null -log10(p-value)", ylab = "qtl  -log10(p-value)", 
                figsize = 3):
    d0_, _ = emp_qq(d1, d0)
    
    if (type(figsize) is tuple) or (type(figsize) is list):
        fig, ax = plt.subplots(1,1, figsize = figsize)
    else:
        fig, ax = plt.subplots(1,1, figsize = (figsize, figsize) )
    ax.plot( d0_,  d1, ls)
    xlims = [ np.floor(d1[0]), np.ceil(d1[-1])]
    plt.plot(xlims, xlims, color = (0,0,0,.4), zorder = -1)
    plt.axis('equal')
    plt.xlim(xlims)
    plt.ylim(xlims)
    plt.xlabel(xlab)
    plt.ylabel(ylab)
    plt.title(title)
    fig.tight_layout()
    return fig, ax

def FDR(thr, intact, permuted, npermutations):
    num_vals_below = lambda x,y: sum(y<x)
    all_positive_rate = num_vals_below(thr, intact)
    false_positive_rate = num_vals_below(thr, permuted ) / npermutations
    return false_positive_rate/all_positive_rate

def fdr(intact, nulld, nperm, k_extrapolation = 1):
    """
    empirical false discovery rate
    intact -- a vector of test p-values
    nulld --  vector of permutation p-values
    nperm -- number of permutations
    k_extrapolation -- polynomial degree of extrapolation
    """
    q = np.ones_like(intact)
    sortinds_intact = np.argsort(intact)
    all_positive_rate = np.argsort(sortinds_intact) +1

#    sortinds_null = np.argsort(nulld)
#    rank_null = np.argsort(sortinds_null)/nperm
    false_positive_rate = np.zeros_like(intact)
    for ii, xx in enumerate(intact):
        false_positive_rate[ii] = np.sum(nulld < xx)/nperm

    q = false_positive_rate/ all_positive_rate
    qz = q == 0
    # q[qz] = min(q[~qz])    
    from scipy.interpolate import UnivariateSpline
    valid = ~qz & all_positive_rate < sum(~qz)//2
    extrapolator = UnivariateSpline( np.log2(intact[valid]),
                                    np.log2(q[valid]), k= k_extrapolation)
    q[qz] = 2** extrapolator( np.log2(intact[qz]) )
    q[np.isnan(q) & qz] = 0
    return q

#def FDRhist(intact, permuted, npermutations):
#    sortinds0 = np.argsort(intact)
#    sortinds1 = np.argsort(sortinds0)
#
#    "Then, to sort the list with such indices, you can do:"
#    intact = np.array(intact)[sortinds0]
#    num = []
#    fdr = []
#    num_vals_below = lambda x,y: sum(y<x)
#    for num_vals_below_intact in range(len(intact)):
#        num.append(num_vals_below_intact)
#        thr = intact[num_vals_below_intact]
#        all_positive_rate = num_vals_below_intact
#        false_positive_rate = num_vals_below(thr, permuted ) / npermutations
#        fdr.append(false_positive_rate/all_positive_rate)
#    return np.array(fdr)[sortinds1], np.array(num)[sortinds1]
