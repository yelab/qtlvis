#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 10:03:30 2015

@author: dlituiev
"""

import pandas as pd
import os, sys

def rna_meta(r_metafile):
    print(r_metafile)
    rmetadf = pd.read_csv(r_metafile, sep = '\t',index_col = 0)
    #rmetadf.columns = ['chr', 'centre']
    cols = list(rmetadf.columns)
    cols[0] = 'chr'
    cols[1] = 'start'
    cols[2] = 'end'
    rmetadf.columns = cols
    #print( rmetadf.head() )
    
    rmetadf['start'] = rmetadf['start'].astype(int)
    rmetadf['end'] = rmetadf['end'].astype(int)
    rmetadf['centre'] = (rmetadf['start'] +  rmetadf['end'] ) / 2
    rmetadf['Mb'] = rmetadf['centre'] * 1e-6
    return rmetadf
    
def gt_meta(a_metafile, window = 0):
    ametadf = pd.read_csv(a_metafile, sep = '\t',index_col = 0)
    #print(ametadf.columns)
    ametadf.columns = ['chr', 'centre']
    ametadf['start'] = ametadf['centre'] - window/2 +1
    ametadf['end'] = ametadf['centre'] + window/2
    ametadf['Mb'] = ametadf['centre'] * 1e-6
    # ametadf.drop('centre', inplace = True, axis = 1)
    return ametadf

def file_or_df(xxx, reader =  lambda x: pd.read_csv(x, sep = '\t')):
    if type(xxx) is str:
        if os.access(xxx, os.R_OK):
            xxx = reader(xxx)
        else:
            raise OSError('file not found: %s' % xxx )
    return xxx

def get_xy(Z, ametadf, rmetadf, 
           x_name = 'CHROMATIN',
           y_name = 'RNA',
           z_name = 'STATS',
           atac_window = 100):
    
    Z = file_or_df(Z)

    zcols = list(Z.columns)
    zcols[0] = 'SNP'
    try:
        zcols[zcols.index('pvalue')] = 'p-value'
    except ValueError:
        pass
    try:
        zcols[zcols.index('qvalue')] = 'q-value'
    except ValueError:
        pass
    Z.columns = zcols
    
    if (Z.iloc[0,0] == 'No significant associations were found.'):
        Z.drop(0, inplace = True)
    Z.reset_index(inplace = True)
    
    rmetadf = file_or_df( rmetadf, rna_meta)
    ametadf = file_or_df( ametadf, lambda x: gt_meta(x, window = atac_window))
    

    X = ametadf.loc[Z['SNP']].copy()
    X.reset_index(inplace = True)
    X.drop(X.columns[0], axis = 1, inplace = True)
    
    Y = rmetadf.loc[Z['gene']].copy()
    Y.reset_index(inplace = True)
    Y.drop(Y.columns[0], axis = 1, inplace = True)
    
    dd = {x_name: X, y_name: Y, z_name: Z}
    #    print([x.shape for x in dd.values()])    
    #    print([x.index.shape for x in dd.values()])
    #    print([x.index.unique().shape for x in dd.values()])
    out = pd.concat(dd.values(), axis = 1, keys= list(dd.keys()) )
    
    cols = out.columns.tolist()
    for n in range(len(cols)):
        if 'SNP' in cols[n]:
            cols[n] = (x_name, 'name')
        elif 'gene' in cols[n]:
            cols[n] = (y_name, 'name')

    out.columns = pd.MultiIndex.from_tuples(cols)
    out.sortlevel(axis = 1, inplace=True)
    
    out[z_name,'-log10 p-value'] = -pd.np.log10(out[z_name,'p-value'])

    print(' out.shape ')
    print( out.shape )

    inds =  pd.np.array(out.isnull().any(axis=0))
    print('nans in columns:')
    print( pd.DataFrame( out.columns.get_values().tolist() )[inds] )

    out.dropna(inplace = True)
    print(' out.shape ')
    print( out.shape )
    return out

if __name__ == '__main__':

    ###############################################################################
    import argparse
    parser = argparse.ArgumentParser('''reads files from matrix eQTL
    (a file for all chromosomes + several permutation files )
    ''')

#    parser.add_argument("indir", nargs='?', type=str,
#            default='/Users/dlituiev/repos/qtl_atac_rna/gt_atac/',
#            help="output file (.csv); for stdout type '-' ")

    parser.add_argument("-a", "--xmeta",  type=str, default = "",
                        help="metafile for x ('genotype') variable")

    parser.add_argument("-b", "--ymeta",  type=str, default = "",
                        help="metafile for y ('phenotype') variable")

    parser.add_argument("-x",  type=str, default= "genotype",
                        help="name of x variable")

    parser.add_argument("-y",  type=str, default= "expression",
                        help="name of y variable")

    parser.add_argument("-o", "--output",  type=str, default= "./img",
                        help="output folder")

    parser.add_argument("-i", "--input", type=str, default= "./",
                        help="input folder")
 
    parser.add_argument("-c", "--cis", type=str, default = "", 
                        help="cis file [overrides `--input`]")

    parser.add_argument("-t", "--trans", type=str, default = "", 
                        help="trans file [overrides `--input`]")

    args = parser.parse_args()
    ###############################################################################

    resdir = args.output
    a_metafile = args.xmeta
    r_metafile = args.ymeta
    pttrn = '.matrix.eqtl.%(cistrans)s.txt' # 'genes.2.matrix.eqtl.%(cistrans)s.txt'
    descr = pd.DataFrame({'data': [args.x, args.y]}, index = ['x', 'y'])
    
    print('saving to: ' , resdir, file = sys.stderr)

    pval_files = {}
    if (len(args.cis)>0) or (len(args.trans)>0):
        pval_files["cis"] = args.cis
        pval_files["trans"] = args.trans
    else:
        lst=os.listdir(args.input)
        ncis = 0
        ntrans = 0
        for ff in lst:
            if ff.endswith(".txt"):
                if ff.endswith( '.matrix.eqtl.cis.txt' ):
                    ncis += 1
                    pval_files["cis"]   = args.input + "/" +  ff 
                if ff.endswith( '.matrix.eqtl.trans.txt' ):
                    ntrans += 1
                    pval_files["trans"] = args.input + "/" +  ff 
        if ncis > 1:
            raise IOError("confused... to many cis files. provide a folder with ONE *.matrix.eqtl.cis.txt file!")
        if ntrans > 1:
            raise IOError("confused... to many trans files. provide a folder with ONE *.matrix.eqtl.trans.txt file!")
        del ncis
        del ntrans

    #################################
    qtls_df = pd.DataFrame()
    r_meta =  rna_meta(r_metafile)
    g_meta =  gt_meta(a_metafile, window = 100)
    for cistrans in ['cis', 'trans']:
        #pval_files[cistrans] = resdir + pttrn % {'chrm': cc, 'cistrans':cistrans}
        
        cis_df = get_xy(pval_files['cis'], g_meta, r_meta,
                        x_name = descr.loc['x'][0],
                        y_name = descr.loc['y'][0])   
        cis_df[('STATS', 'cis')] = True
        
        trans_df = get_xy(pval_files['trans'], g_meta, r_meta,
                        x_name = descr.loc['x'][0],
                        y_name = descr.loc['y'][0])
        trans_df[('STATS', 'cis')] = False
        
        qtls_df = qtls_df.append(  pd.concat([cis_df, trans_df], axis = 0) )
        
        # print('read chromosome %s' % cc, file = sys.stderr)
        print('\tdata frame size: ', qtls_df.shape, file = sys.stderr)
        if cis_df.shape[0] == 0:
            print('\tno cis QTLS !!!', file=sys.stderr)
        
    del trans_df, cis_df, g_meta, r_meta
    
    if not qtls_df['STATS','cis'].any():
        print('no cis QTLS !!!', file=sys.stderr)
    else:
        print(  '%u cis' % qtls_df['STATS','cis'].sum(), file = sys.stderr )
    
    "save df"
    with pd.HDFStore(resdir + '/qtls.h5',  mode='w') as store:
        print("saving to a HDF5", file = sys.stderr)
        store.append('qtls_df', qtls_df, format='table')
        store.append('description', descr, format='table')
        
    #counts = qtls_df[[(descr['y'],"name"),(descr['y'],"end")]].groupby([(descr['y'],"name")]).agg('count')
    #counts = counts.sort(columns = (descr['y'],"end")).iloc[:,0]
    #
    #counts.index.get_values()
    #qtls_df.set_index((descr['y'],"name")).loc[counts.index.get_values()]
