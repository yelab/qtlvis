import pandas as pd

def to_padded(x, n = 3):
    if x == 'Un':
        return '99'+ x
    try:
        return ('%%0%uu' % n) % int(x)
    except:
        try:
            int(x[0])
            return x
        except:
            return '1'+ x

def chr_pos_plain(inpair, chrsizefile= 'hg19.chrom.sizes'):

#    chrorder = [ 'chr%s' % x for x in range(1,22+1) ]
#    chrorder.append('chrX')
#    chrorder.append('chrY')
#    chrorder.append('chrM')

#    pd.np.unique(pd.np.sort(tok))
    
    chrsizes = pd.read_csv(chrsizefile, sep = '\t', index_col = 0, header = -1)
    chrsizes.columns=['size']

    chrsizes.index.tolist()
    tok = [ x.split('_')[0].lstrip('chr') for x in chrsizes.index.tolist()]
    chrsizes['token'] = [to_padded(t) for t  in tok]
    chrsizes.sort('token', inplace = True)
    chrorder = chrsizes['token']
    
    chrsizes['cum_start'] = pd.np.cumsum(chrsizes['size']) - chrsizes['size']

    
    chr_ind = lambda x: pd.np.where(chrsizes['token'] == x)

    plain = []
    if type(inpair) == pd.core.frame.DataFrame:
        inpair = inpair.as_matrix()
    for p in inpair:
        plain.append(  chr_ind(p[0]) + p[1]  )

    return plain

if __name__ == "__main__":
    import sys
    chr_pos_plain(sys.argv[1:])




import matplotlib.pyplot as plt
import matplotlib as mpl


# fig.show()

mpl.patches.Patch
