#!/bin/bash

# sed -re 's/(^PC[0-9]*)\t/\1_atac\t/g'

npcs_atac=$1
npcs_rna=$2

basic_covariates="covariates/sources/basic.covariates.matrix.eqtl.tab.matched"
rna_pcs_f="covariates/rna.genes.matrix.eqtl.matched.labelled.pcs"
atac_pcs_f="covariates/atac.matrix.eqtl.matched.labelled.pcs"
output="covariates/fused.a${npcs_atac}.r${npcs_rna}.covariates.tab"
scrdir="/netapp/home/dlituiev/qtl_atac_rna"

( [ -r $output ]  ) && (echo "clearing the output file: $output" ; rm $output)

cat $basic_covariates >> $output
"$scrdir"/fromto.sh $atac_pcs_f 2 $((1 + $npcs_atac)) >> $output
"$scrdir"/fromto.sh $rna_pcs_f 2 $((1 + $npcs_rna)) >> $output

echo "saved to:" >&2
echo "$output" >&2
