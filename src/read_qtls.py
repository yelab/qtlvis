# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 10:03:30 2015

@author: dlituiev
"""

import pandas as pd
import os, sys

def rna_meta(r_metafile):
    rmetadf = pd.read_csv(r_metafile, sep = '\t',index_col = 0)
    cols = list(rmetadf.columns)
    cols[0] = 'chr'
    rmetadf.columns = cols
    rmetadf['centre'] = (rmetadf['start'] +  rmetadf['end'] ) / 2
    rmetadf['Mb'] = rmetadf['centre'] * 1e-6
    return rmetadf
    
def gt_meta(a_metafile, window = 0):
    ametadf = pd.read_csv(a_metafile, sep = '\t',index_col = 0)
    ametadf.columns = ['chr', 'centre']
    ametadf['start'] = ametadf['centre'] - window/2 +1
    ametadf['end'] = ametadf['centre'] + window/2
    ametadf['Mb'] = ametadf['centre'] * 1e-6
    # ametadf.drop('centre', inplace = True, axis = 1)
    return ametadf

def get_xy(Z, ametadf, rmetadf, 
           x_name = 'CHROMATIN',
           y_name = 'RNA',
           z_name = 'STATS',
           atac_window = 100):
    def file_or_df(xxx, reader =  lambda x: pd.read_csv(x, sep = '\t')):
        if type(xxx) is str:
            if os.access(xxx, os.R_OK):
                xxx = reader(xxx)
            else:
                raise OSError('file not found: %s' % xxx )
        return xxx
    
    Z = file_or_df(Z)
    if (Z.ix[0,0] == 'No significant associations were found.'):
        Z.drop(0, inplace = True)
    
    rmetadf = file_or_df( rmetadf, rna_meta)
    ametadf = file_or_df( ametadf, lambda x: gt_meta(x, window = atac_window))        
    
    X = ametadf.loc[Z['SNP']].copy()
    X.reset_index(inplace = True)
    X.drop(X.columns[0], axis = 1, inplace = True)
    
    Y = rmetadf.loc[Z['gene']].copy()
    Y.reset_index(inplace = True)
    Y.drop(Y.columns[0], axis = 1, inplace = True)
    
    dd = {x_name: X, y_name: Y, z_name: Z}
    out = pd.concat(dd.values(), axis = 1, keys= list(dd.keys()) )
    
    cols = out.columns.tolist()
    for n in range(len(cols)):
        if 'SNP' in cols[n]:
            cols[n] = (x_name, 'name')
        elif 'gene' in cols[n]:
            cols[n] = (y_name, 'name')
    out.columns = pd.MultiIndex.from_tuples(cols)
    out.sortlevel(axis = 1, inplace=True)
    
    out[z_name,'-log10 p-value'] = -pd.np.log10(out[z_name,'p-value'])

    out.dropna(inplace = True)
    return out

#############################################################
#
#resdir = '/Users/dlituiev/repos/qtl_atac_rna/gt_rna/'
#a_metafile = resdir + 'genotypes.meta'
#r_metafile = resdir + 'rna.genes.meta'
#pttrn = 'baseline.genes.10.chr%(chrm)s.matrix.eqtl.%(cistrans)s.txt' 
#descr = pd.DataFrame({'data': ['genotypes','RNA']}, index = ['x', 'y'])


#resdir = '/Users/dlituiev/repos/dnase_qtl_rna/results/orig.dnase_rna.a0.r0/'
#pttrn = 'orig.dnase_rna.a0.r0.chr%(chrm)s.matrix.eqtl.%(cistrans)s.txt' 
#r_metafile = '/Users/dlituiev/repos/dnase_qtl_rna/in_data/expressions.genes.yri.meta'
#a_metafile = '/Users/dlituiev/repos/dnase_qtl_rna/in_data/orig.dnase.meta'
#descr = pd.DataFrame({'data': ['DNase','RNA']}, index = ['x', 'y'])

##
#resdir = '/Users/dlituiev/repos/qtl_atac_rna/results/rank.cisw1e5.a0.r0/'
##resdir = '/Users/dlituiev/repos/qtl_atac_rna/results/cisw1e5.a0.r0/'
#r_metafile = '/Users/dlituiev/repos/qtl_atac_rna/in_data/rna.genes.meta'
#a_metafile = '/Users/dlituiev/repos/qtl_atac_rna/in_data/atac.meta'
#pttrn = 'qtl_atac_rna.rank.cisw1e5.a0.r0.chr%(chrm)s.matrix.eqtl.%(cistrans)s.txt' 
#descr = pd.DataFrame({'data': ['ATAC','RNA']}, index = ['x', 'y'])

resdir = '/Users/dlituiev/repos/qtl_atac_rna/gt_atac/'
a_metafile = resdir + 'genotypes.meta.tab'
r_metafile = resdir + 'unique.peak.expressions.meta'
pttrn = 'genes.5.chr%(chrm)s.matrix.eqtl.%(cistrans)s.txt'
descr = pd.DataFrame({'data': ['genotype','ATAC']}, index = ['x', 'y'])

#resdir = '/Users/dlituiev/repos/qtl_atac_rna/gt_rna/'
#a_metafile = resdir + 'genotypes.meta'
#r_metafile = resdir + 'rna.genes.meta'
#pttrn = 'genes.10.chr%(chrm)s.matrix.eqtl.%(cistrans)s.txt'
#descr = pd.DataFrame({'data': ['genotypes','RNA']}, index = ['x', 'y'])

#################################
qtls_df = pd.DataFrame()
r_meta =  rna_meta(r_metafile)
g_meta =  gt_meta(a_metafile, window = 100)
for cc in range(1,23):
    pvf = {}
    for cistrans in ['cis', 'trans']:
        pvf[cistrans] = resdir + pttrn % {'chrm': cc, 'cistrans':cistrans}
    
    cis_df = get_xy(pvf['cis'], g_meta, r_meta,
                    x_name = descr.loc['x'][0],
                    y_name = descr.loc['y'][0])   
    cis_df[('STATS', 'cis')] = True
    
    trans_df = get_xy(pvf['trans'], g_meta, r_meta,
                    x_name = descr.loc['x'][0],
                    y_name = descr.loc['y'][0])                    
    trans_df[('STATS', 'cis')] = False
    
    qtls_df = qtls_df.append(  pd.concat([cis_df, trans_df], axis = 0) )
    
    print('read chromosome %s' % cc, file = sys.stderr)
    print('\tdata frame size: ', qtls_df.shape, file = sys.stderr)
    if cis_df.shape[0] == 0:
        print('\tno cis QTLS !!!', file=sys.stderr)
    
del trans_df, cis_df, g_meta, r_meta

if not qtls_df['STATS','cis'].any():
    print('no cis QTLS !!!', file=sys.stderr)
else:
    print(  '%u cis' % qtls_df['STATS','cis'].sum(), file = sys.stderr )

"save df"
with pd.HDFStore(resdir + '/qtls.h5',  mode='w') as store:
    store.append('qtls_df', qtls_df, format='table')
    store.append('description', descr, format='table')
    
