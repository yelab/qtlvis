#!/usr/bin/env python3
from __future__ import print_function
import sys
import numpy  as np
import pandas as pd
import matplotlib.pyplot as plt

def cumhist_logpval(logpval, imgfile, format = "eps", dpi = 300):
    slogpval = np.sort(logpval)
    #imgfile = imgfile + ("" if imgfile.endswith(format) else "."+format )
    if not imgfile.endswith(format):
        imgfile += ("."+format)

    fig = plt.figure()
    plt.plot(  slogpval, np.arange(len(slogpval)), )
    plt.yscale("log")
    plt.grid("on", linestyle='-', color = [0.7]*3)
    plt.grid("on", which='minor', linestyle=':', color = [0.7]*3)
    plt.xlabel("log10(p-value)")
    plt.ylabel("cumulative number of hits")
    plt.savefig(imgfile , format = format, dpi = dpi)

    print("see", imgfile, sep="\t", file = sys.stderr)
    return fig

if __name__ == '__main__':
    ###############################################################################
    import argparse
    parser = argparse.ArgumentParser('''plotter for footprints ''')

    parser.add_argument("infile", type=str,
            help="input file (.tab); for stdout type '-' ")

    parser.add_argument("-f", "--format", type= str, default= 'png',
                        help="graphics format (png, jpg, eps, pdf)")

    parser.add_argument("-x", "--scale", type= str, default= None,
                        help="x-scale (None, atan)")

    parser.add_argument("-s", "--figsize", type= float, default= None,
                        help="figure size (side) in inches")

    parser.add_argument("-d", "--dpi", type= int, default= 300,
                        help="resolution [dpi]")

    args = parser.parse_args()
    ###############################################################################
    infile = args.infile

    assert ( infile.endswith(".txt") and (".matrix.eqtl." in infile)), "please provide a .matrix.eqtl.*.txt file"

    imgfile = infile.replace(".txt", ".logpval.cumhist")
    summaryfile = infile.replace(".txt", ".summary.txt")

    df = pd.read_table(infile)

    logpval = df["p-value"].apply(pd.np.log10)

    plt.close()
    cumhist_logpval(logpval, imgfile, args.format, dpi = args.dpi)

    with open(summaryfile, "w+") as f:
        print("log10 p-value", "number of hits" , sep = "\t", file = f)
        for tt in range( 1+ int(np.floor(min(logpval))), 1+int(np.floor(max(logpval))) ):
            print(tt, sum(logpval<tt) , sep = "\t", file = f)

print("see", summaryfile, sep = "\t", file = sys.stderr)
