#!/usr/bin/python
from __future__ import print_function
import sys
from runshell import *
# print 'Number of arguments:', len(sys.argv), 'arguments.'

"empty cells are skipped by awk if not set: FS = \t"

"read the headers"
samples = [[],[]]
for n in [0,1]:
    with open(sys.argv[n+1]) as f:
        line = f.readline().rstrip('\n')
        samples[n] = line.split('\t')
        print(samples[n], file = sys.stderr )
        print('length of the sample %u: %u' % ( n, len(samples[n]) ) , file = sys.stderr )

inters = list( set(samples[0]) & set(samples[1]) )
inters.sort()
print('length of the intersection: %u' % len(inters) , file = sys.stderr)
print('intersection:', file = sys.stderr)
print(inters, file = sys.stderr)


validlist = [[],[]]
for x in inters:
    for n in [0,1]:
        ix = samples[n].index(x)
        validlist[n].append( ix )
#        print('sample %u' % n, 'index %2u' % ix, 'value %s' % samples[n][ix], file = sys.stderr)
        ix = None

for n in [0,1]:
    print('valid columns in sample %u ( %s ):' % (n, sys.argv[n+1]) , file = sys.stderr)
    print(validlist[n], file = sys.stderr)
    print('', file = sys.stderr)


assert ( len(validlist[0]) == len(validlist[1]) ), "list lengths do not match"

for n in [0,1]:
    clmnlist = ','.join(['$%u' % (x + 1) for x in validlist[n] ] )
    cmd = """awk 'BEGIN{ FS = "\\t"; OFS="\\t"} {print $1,%(clmns)s}' %(infile)s > %(outfile)s""" % \
        {"clmns": clmnlist, "infile": sys.argv[n+1], "outfile": sys.argv[n+1]+".matched" }
    print("running command:", file = sys.stderr)
    print(cmd, file = sys.stderr)
    out = runcmd(cmd)
    print("returned[stderr]: ", out[1], file = sys.stderr)
    


