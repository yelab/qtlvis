#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 28 17:42:18 2015

@author: dlituiev
"""
import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import matplotlib.gridspec as gridspec
import pdscatter as pdsc

import matplotlib.transforms as transforms

#import warnings
#warnings.filterwarnings('error')
###############################################################################
###############################################################################
"for multiple chromosomes"    
def decimate_spl_labels(axes):
    for ax in axes[:-1,:].ravel():
        if ax is not None:
        # a.plot([0,1],[0,1], 'r-')
            ax.set_xticklabels('')
            ax.set_xlabel('')
        else:
            print('None instead of ax object!', file = sys.stderr)
    for ax in axes[:, 1:].ravel():
        if ax is not None:
            ax.set_yticklabels('')
            ax.set_ylabel('')
        else:
            print('None instead of ax object!', file = sys.stderr)
#    for ax in axes[:, 0].ravel():
#        labels = [item.get_text() for item in ax.get_xticklabels()]
#        labels[-1] = ''
#        ax.set_xticklabels(labels)
#    for ax in axes[ -1, :].ravel():
#        labels = [item.get_text() for item in ax.get_yticklabels()]
#        labels[-1] = ''
#        ax.set_yticklabels(labels)

###############################################################################
# cm = plt.cm.get_cmap('RdYlBu')
###############################################################################
def get_chromosome_size(test_df):
    return pd.concat( [test_df.groupby([(x_name, 'chr')])[[(x_name, 'Mb')]].max(),
           test_df.groupby([(y_name, 'chr')])[[(y_name, 'Mb')]].max()], axis = 1).max(axis = 1)
###############################################################################
def scatter_interactions(test_df, x_name, y_name, zz, chrlist = [],
                         blacklist = None, legendtitle = None,
                         nchrom = 22, figsize = 10, grid_step = 50,
                         grid_max = 301, dot_size_max = 10, gray_c = 0.75, 
                         alphamin = 1/3, alphamax= 1/3,
                         alpha = None,
                         legendstep = 1, legendoffset = 0.1,
                         labelfontsize = 12):
    
    if alpha:
        alphamin = alpha
        alphamax = alpha

    points_per_inch = 72
    font_aspect_ratio = 0.6
    points_to_inch = lambda fontsize :  fontsize /points_per_inch
    symbol_width_inch = lambda fontsize : points_to_inch(fontsize) *font_aspect_ratio
    
    def axlabel_fs(axwidthpt, xlabel, fontsize = 12):
        label_width_inch = symbol_width_inch(fontsize) *(len(xlabel) + 2)
        if axwidthpt < label_width_inch:
            fontsize *= axwidthpt / label_width_inch
        return fontsize
    
    vmin = test_df[zz].min()
    vmax = test_df[zz].max()
    chrsize = get_chromosome_size(test_df)
    if len(chrlist) == 0:
        chrlist = ['chr%s' % (1+x) for x in range(0, nchrom)]
    uniq_chr =  set(test_df[(x_name, 'chr')].unique()) | set(test_df[(y_name, 'chr')].unique())

    loginds = [ cc in uniq_chr for cc in chrlist]
    chrlist = [ v for i, v in enumerate(chrlist) if loginds[i] ]
    
    for cc in range(len(chrlist)):
        if chrlist[cc] not in uniq_chr:
            chrlist.pop(cc)
    
    chrorder = dict(zip(chrlist, range(0, len(chrlist))))
    chrsize = chrsize[chrlist]
    chrsize.dropna(inplace = True)
    
    nlegendsymbols = 5
    legendwidth_inch =  points_to_inch( dot_size_max ) + nlegendsymbols * symbol_width_inch(labelfontsize)
    legendwidth_fraction =  legendwidth_inch/figsize
    
    fig = plt.figure(figsize=(figsize, figsize))
    gs = gridspec.GridSpec(chrsize.shape[0], chrsize.shape[0] + 1,
                           width_ratios= chrsize.tolist() + [legendwidth_fraction*chrsize.sum()],
                           height_ratios= pd.np.flipud(chrsize.as_matrix()) )
    axs = pd.np.empty([chrsize.shape[0], chrsize.shape[0]], dtype =plt.matplotlib.axes.SubplotBase)
    # fig, axs = plt.subplots(N_CHROMOSOMES,N_CHROMOSOMES, figsize=(L, L), facecolor='w', edgecolor='k')
    plt.subplots_adjust( hspace = 0.02, wspace = 0.02 )
    CURSOR_UP_ONE = '\x1b[1A'
    ERASE_LINE = '\x1b[1M'
    print('plotting ', file=sys.stderr)    
    for xn in chrlist:
        for yn in chrlist:
            print(CURSOR_UP_ONE + ERASE_LINE +  CURSOR_UP_ONE, file=sys.stderr)
            print('plotting: %s, %s' % (xn, yn), file = sys.stderr)
            ax = plt.subplot(gs[chrsize.shape[0] -1 - chrorder[yn], chrorder[xn]])
            axs[chrsize.shape[0] -1 - chrorder[yn], chrorder[xn]] = ax
            # ax = axs[chrorder[yn], chrorder[xn]]
            inds = ((test_df[(x_name, 'chr')] == xn) & (test_df[(y_name, 'chr')] == yn))
            fig, ax, _ = pdsc.scatter_df(test_df[inds], 
                  (x_name,'Mb'), (y_name,'Mb'), zz,
                correlate = False, fig=fig, ax = ax, colorbar = False,
                alphamin = alphamin, alphamax= alphamax,
                clim = [vmin, vmax], size_max = dot_size_max, horizontal = None)
            major_ticks = pd.np.arange(0, grid_max, grid_step) 
            if yn == xn:
                ax.plot([0, chrsize.loc[xn] ], [0, chrsize.loc[xn] ], color = [gray_c]*3, zorder= -1 )
            
            ax.set_xticks(major_ticks)
            ax.set_yticks(major_ticks)
            xoverlapping = ax.get_xlim()[-1] - ax.get_xticks() < grid_step # *3 / 4
            labels = np.array([ '%u' % s for s in major_ticks])
            labels[xoverlapping] = ''
            ax.set_xticklabels(labels, rotation=90)
            ax.set_yticklabels(labels, rotation=0)
            ax.set_title('')
            ax.set_xlim([0, chrsize.loc[xn] ])
            ax.set_ylim([0, chrsize.loc[yn] ])
#            axwidth = ax.get_position().width
#            axwidthpt = fig.get_figwidth()*axwidth*72
            # def adjust_fontsize(axdim)
            # print('axes width:', axwidthpt, width*72 , file = sys.stderr)
            bbox = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
            ax.set_ylabel(yn, fontsize = axlabel_fs(bbox.height, yn, fontsize = labelfontsize) )# + ' : ' + y_name )
            ax.set_xlabel(xn, fontsize = axlabel_fs(bbox.width, xn, fontsize = labelfontsize) )# + ' : ' + x_name)
            # print(ax.__repr__(), xn, yn )
            ax.xaxis.labelpad = 20
            ax.yaxis.labelpad = 20
    
    if (vmax-vmin)/legendstep > 15:
        legendstep = round((vmax-vmin)/10 )
        print("legend step reset to ", legendstep , file = sys.stderr)

    leg_oom = int(round(np.log10(legendstep)))
    formatter = '%%.%uf' % leg_oom
    
    calibration = np.arange(vmin, vmax, legendstep)
    dflegend  = pd.DataFrame({"size": calibration,
                              'x': np.zeros_like(calibration),
                              'y': np.cumsum(calibration + legendoffset) - vmin })
    ax = plt.subplot(gs[:, -1])
    fig, ax, _ = pdsc.scatter_df(dflegend, 'x', 'y', 'size',
                correlate = False, fig=fig, ax = ax, colorbar = False,
                alphamin = alphamin, alphamax = alphamax,
                clim = [vmin, vmax], size_max = dot_size_max, horizontal = None)
    ax.set_xticks([])
    ax.set_yticks(dflegend['y'].tolist())
    ax.set_yticklabels( dflegend['size'].map( lambda x : formatter % x).tolist() )
    ax.yaxis.tick_right()
    ax.grid('off')
    ax.set_title('')
    ax.set_xlabel('')
    if not legendtitle:
        legendtitle = zz
    ax.set_ylabel(legendtitle)
    ax.yaxis.set_label_position("right")
    
    for sp in ['top', 'bottom', 'left', 'right']:
        ax.spines[sp].set_visible(False)
    
    "shade the black list regions here"
    if blacklist is not None:
        zuerigrau = [gray_c + (1 - gray_c)/4,]*3
        for xn in chrlist:
            for yn in chrlist:
                ax = axs[chrsize.shape[0] -1 - chrorder[yn], chrorder[xn]]
                for _, region in blacklist[blacklist['chr'] == 'chr1'].iterrows():
                    start_, end_ = 1e-6*region['start'], 1e-6*region['end']
                    width_ = end_ - start_
                    midpointx = (end_ + start_)/2
                    height_ = chrsize.loc[yn]
                    ax.add_patch(Rectangle( (midpointx, 0), 
                       width_, height_, 
                       facecolor=zuerigrau, edgecolor=zuerigrau, linewidth=0.5, zorder = -3))
        
    decimate_spl_labels(axs)
    fig.suptitle('x: %s, y: %s;' % (x_name, y_name) +
    ' %s range: ' % zz[-1] + 
    '[%.2f, %.2f]' % (vmin, vmax), fontsize = 18)
    return fig, axs
###############################################################################
def hist_trans(test_df, x_name, y_name, figsize = 4, chrlist = [], nchrom = 22, ntbin = 1e6):
    chrsize = get_chromosome_size(test_df)
    if len(chrlist) == 0:
        chrlist = ['chr%s' % (1+x) for x in range(0, nchrom)]
    chrorder = dict(zip(chrlist, range(0, len(chrlist))))
    
    trans_inds = test_df[(y_name, 'chr')] !=  test_df[(x_name, 'chr')]
    print('fraction of truly trans qtls:', trans_inds.sum() / trans_inds.shape[0], file = sys.stderr)
    fig = plt.figure(figsize=(figsize,  0.5 * figsize / chrsize.shape[0] ))
    fig.suptitle(x_name + ', loci in trans')
    gs = gridspec.GridSpec( 1, chrsize.shape[0],
                            width_ratios = chrsize.as_matrix(),
                            height_ratios= [chrsize.median()] )
    axs = []
    zmin = np.floor(test_df[('STATS', z_variable)].min())
    zmax = np.ceil(test_df[('STATS', z_variable)].max())

    nbins = zmax - zmin + 1
    xh = np.linspace(zmax, zmin, nbins)

#    hi = test_df[('STATS', z_variable)].groupby(grper).agg( \
#            lambda x: tuple(np.histogram(x, range = (minz, maxz) , bins = nbins)[0]) )
    CURSOR_UP_ONE = '\x1b[1A'
    ERASE_LINE = '\x1b[2K'
    print('plotting ', file=sys.stderr)
    for xn, g in test_df[trans_inds].groupby([(x_name, 'chr')], axis=0):
        grper = qtls_df[(x_name,'centre')].apply( lambda x: np.floor(x / ntbin) )
        hi = g[('STATS', z_variable)].groupby(grper).agg( \
            lambda x: tuple(np.histogram(x, range = (zmin, zmax) )[0]) )
        himatr = np.array(hi.tolist()).transpose()
        print('chr', xn, ', number of loci: ' ,g.shape[0] )
        ax = fig.subplot(gs[0, chrorder[xn]])
        axs.append( ax )
        ax.pcolor(np.array(hi.index*(ntbin/1e-6), xh, himatr))
        plt.ylabel(z_variable)
        plt.xlabel(xn + ', Mb')
        ax.set_xlim([0, chrsize.loc[xn] ])
        ax.set_ylim([zmin, zmax])
        print(CURSOR_UP_ONE + ERASE_LINE, file=sys.stderr)
        print('plotted ', xn, file=sys.stderr)
    # plt.tight_layout()
    # decimate_spl_labels(pd.np.array(axs)[pd.np.newaxis])
    plt.subplots_adjust( hspace = 0.02, wspace = 0.02 , bottom=0.15)
    return fig, axs
###############################################################################
def plot_trans(test_df, x_name, y_name, figsize = 4, chrlist = [], nchrom = 22):
    chrsize = get_chromosome_size(test_df)
    if len(chrlist) == 0:
        chrlist = ['chr%s' % (1+x) for x in range(0, nchrom)]
    chrorder = dict(zip(chrlist, range(0, len(chrlist))))
    
    trans_inds = test_df[(y_name, 'chr')] !=  test_df[(x_name, 'chr')]
    print('fraction of truly trans qtls:', trans_inds.sum() / trans_inds.shape[0])
    fig = plt.figure(figsize=(figsize,  0.5 * figsize / chrsize.shape[0] ))
    fig.suptitle(x_name + ', loci in trans')
    gs = gridspec.GridSpec( 1, chrsize.shape[0],
                            width_ratios = chrsize.as_matrix(),
                            height_ratios= [chrsize.median()] )
    axs = []
    for xn, g in test_df[trans_inds].groupby(test_df[trans_inds][x_name, 'chr'], axis=0):
        print('chr', xn, ', number of loci: ' ,g.shape[0] )
        ax = plt.subplot(gs[0, chrorder[xn]])
        axs.append( ax )
        plt.scatter(g[(x_name, 'Mb')], g[('STATS', z_variable)])
        plt.ylabel(z_variable)
        plt.xlabel(xn + ', Mb')
        ax.set_xlim([0, chrsize.loc[xn] ])
    # plt.tight_layout()
    decimate_spl_labels(pd.np.array(axs)[pd.np.newaxis])
    plt.subplots_adjust( hspace = 0.02, wspace = 0.02 , bottom=0.15)
    return fig, axs
###############################################################################
def scatter_cis(test_df, x_name, y_name, zz, chrlist = [],
                     nchrom = 22, figsize = 10, grid_step = 50,
                     grid_max = 301, size_max = 10, gray_c = 0.85, window = 1e6):

    cis_inds = (test_df[(y_name, 'chr')] !=  test_df[(x_name, 'chr')])
    test_df[cis_inds][('STATS', 'offset')] = qtls_df[(x_name, 'centre')] - qtls_df[(y_name, 'centre')]
    
    vmin = test_df[cis_inds][zz].min()
    vmax = test_df[cis_inds][zz].max()
    chrsize = get_chromosome_size(test_df)
    if len(chrlist) == 0:
        chrlist = ['chr%s' % (1+x) for x in range(0, nchrom)]
    
    chrorder = dict(zip(chrlist, range(0, len(chrlist))))
    
    fig = plt.figure(figsize=(figsize, figsize))
    gs = gridspec.GridSpec(chrsize.shape[0], chrsize.shape[0],
                           width_ratios= chrsize.as_matrix(),
                           height_ratios= pd.np.flipud(chrsize.as_matrix()) )
    axs = pd.np.empty([chrsize.shape[0], chrsize.shape[0]], dtype =plt.matplotlib.axes.SubplotBase)
    # fig, axs = plt.subplots(N_CHROMOSOMES,N_CHROMOSOMES, figsize=(L, L), facecolor='w', edgecolor='k')
    plt.subplots_adjust( hspace = 0.02, wspace = 0.02 )
    for xn, g in test_df.groupby(test_df[x_name, 'chr'], axis=0):            
            print('plotting: %s, %s' % (xn), file = sys.stderr)
            ax = plt.subplot(gs[chrsize.shape[0] -1 - chrorder[yn], chrorder[xn]])
            axs[chrsize.shape[0] -1 - chrorder[yn], chrorder[xn]] = ax
            # ax = axs[chrorder[yn], chrorder[xn]]
            fig, ax, _ = pdsc.scatter_df(h, 
                  (x_name,'Mb'), (y_name,'Mb'), zz,
                correlate = False, fig=fig, ax = ax, colorbar = False,
                clim = [vmin, vmax], size_max = size_max, horizontal = None)
            major_ticks = pd.np.arange(0, grid_max, grid_step) 
            if yn == xn:
                ax.plot([0, chrsize.loc[xn] ], [0, chrsize.loc[xn] ], color = [gray_c]*3, zorder= -1 )
            ax.set_xticks(major_ticks)
            ax.set_yticks(major_ticks)
            ax.set_title('')
            ax.set_xlim([0, chrsize.loc[xn] ])
            ax.set_ylim([0, chrsize.loc[yn] ])
            ax.set_ylabel(yn + ' : ' + y_name )
            ax.set_xlabel(xn + ' : ' + x_name)
            # print(ax.__repr__(), xn, yn )
    
    decimate_spl_labels(axs)
    return fig, axs
###############################################################################
if __name__ == '__main__':
    ###############################################################################
    import argparse
    parser = argparse.ArgumentParser(
    '''plotter for QTL matrices [from binary hdf5 files]
    ''')

    parser.add_argument("resultdir", nargs='?', type=str, 
            default='./',
            help="result folder")
    
    parser.add_argument("-n", "--n_chromosomes", type=int, default= 0,
                        help="plot first xn chromosomes (test mode)")
    
    parser.add_argument("-x", "--chromosomes", type=list, action='append',
                         help="plot specified chromosomes")

    parser.add_argument("-f", "--format", type= str, default= 'png',
                        help="graphics format (png, jpg, eps, pdf)")

    parser.add_argument("-s", "--figsize", type= float, default= 20,
                        help="figure size (side) in inches")

    parser.add_argument("-t", "--threshold", type= float, default= 7,
                        help="-log_10 p-value threshold")

    parser.add_argument("-d", "--dpi", type= int, default= 300,
                        help="resolution [dpi]")    

    parser.add_argument("-b", "--ntbin", type= int, default= 7,
                        help="-bin [nt] for p-value pooling")    

    parser.add_argument("-a", "--alpha", type=float, default= 1/3,
                        help="alpha transparency value for dots ")    

    parser.add_argument("-l", "--blacklist", type= str, default= None,
                        help="blacklist file")

    parser.add_argument("-z", "--statistic", type= str, default= "-log10 p-value",
                        help="statistic to plot") 


    args = parser.parse_args()
    ###############################################################################
    resdir = args.resultdir
    print('working on: ' , resdir, file = sys.stderr)
    
    if args.chromosomes:
        chromosomes_to_plot = [''.join(x) for x in args.chromosomes]
        print(chromosomes_to_plot)
    ##################################################################
    z_variable = args.statistic
    
    if args.blacklist and  len(args.blacklist)>0:
        blacklist = pd.read_csv(args.blacklist, sep = '\t')
        blacklist = blacklist.ix[:,:3]
    else:
        blacklist = None
    ##################################################################
    with pd.HDFStore(resdir + '/qtls.h5',  mode='r') as store:
        qtls_df = store.select('qtls_df')
        descr = store.select('description')
        if not qtls_df['STATS','cis'].any():
            print('no cis QTLS !!!', file=sys.stderr)
        else:
            print(  '%u cis' % qtls_df['STATS','cis'].sum(), file = sys.stderr )
    
    x_name = descr.loc['x'][0]
    y_name = descr.loc['y'][0]
    
#    if "chrom" in qtls_df[x_name].columns:
#        lev1 = [{'chrom':'chr'}.get(x) for x in qtls_df.columns.get_level_values(1).tolist()]
#        qtls_df.index.set_levels(qtls_df.columns.get_level_values(0), lev1, inplace=True)
#        # qtls_df.rename(columns = {'chrom':'chr'})
#        print(qtls_df.columns)
#        
    ##################################################################
    print( 'number of truly trans before threshold: \t %u' % (qtls_df[(x_name,'chr')] != qtls_df[(y_name,'chr')]).sum() )
    print( 'number of trans (incl. far cis) before threshold: \t %u' % (qtls_df[('STATS','cis')]==False).sum() )
    qtls_df =  qtls_df[ qtls_df[('STATS', z_variable)] > args.threshold ]
    print( 'number of truly trans after threshold: \t %u' % (qtls_df[(x_name,'chr')] != qtls_df[(y_name,'chr')]).sum() )
    print( 'number of trans after threshold: \t %u' % (qtls_df[('STATS','cis')]==False).sum() )
    qtls_df.reset_index(drop = True, inplace = True)
    ##################################################################
    chrlist = ['chr%s' % (1+x) for x in range(0, 22)]
    chrlist.append('chrX')
    chrlist.append('chrY')
    chrlist.append('chrM')
    ##################################################################
    if args.n_chromosomes:
        "test mode"
        inds = pd.np.zeros(qtls_df[x_name, 'chr'].shape, dtype = bool)
        for x in chrlist[0:args.n_chromosomes]:
            for y in chrlist[0:args.n_chromosomes]:
                 inds = inds | \
                     ( (qtls_df[x_name, 'chr'] == '%s' % (x) ) & \
                       (qtls_df[y_name, 'chr'] == '%s' % (y) ) )
                 
        test_df = qtls_df[inds].copy()
        test_df.reindex()        
        qtls_df = test_df
        print( 'number of truly trans after chromosome subsetting: \t %u' % (qtls_df[(x_name,'chr')] != qtls_df[(y_name,'chr')]).sum() , file = sys.stderr)
    chrs_suffix =  '%s_chr' % args.n_chromosomes if args.n_chromosomes else 'all_chr'
    ##################################################################
    
    if args.chromosomes:
        inds = pd.np.zeros(qtls_df[x_name, 'chr'].shape, dtype = bool)
        allchromosomes = set(qtls_df[x_name, 'chr'].tolist() ) | set( qtls_df[y_name, 'chr'].tolist() )
        chromosomes_to_plot = list( filter( lambda x : x in allchromosomes,  chromosomes_to_plot) )
        if len(chromosomes_to_plot) < len(args.chromosomes):
            print("some chromosomes were dropped from the list", file = sys.stderr )
        if len(chromosomes_to_plot):
            for x in chromosomes_to_plot:
                for y in chromosomes_to_plot :
                     inds = inds | \
                         ( (qtls_df[x_name, 'chr'] == '%s' % (x) ) & \
                           (qtls_df[y_name, 'chr'] == '%s' % (y) ) )

            test_df = qtls_df[inds].copy()
            test_df.reindex()        
            qtls_df = test_df
            print( 'number of truly trans after chromosome subsetting: \t %u' % (qtls_df[(x_name,'chr')] != qtls_df[(y_name,'chr')]).sum() , file = sys.stderr)

            chrs_suffix = '_'.join(chromosomes_to_plot)
        else:
            print("no of provided chromosomes is in the chromosome list", file = sys.stderr )

    ##################################################################
#    out_name = resdir + x_name + chrs_suffix +'.' + args.format
#    fig, axs = hist_trans(qtls_df, x_name, y_name,  figsize = args.figsize, chrlist = chrlist, ntbin = args.ntbin)
#    fig.savefig(out_name , dpi = 300, format = args.format)
#    print('see :' , out_name, sep = '\t', file = sys.stderr)
#    ##################################################################
#    fig, axs = plot_trans(qtls_df, x_name, y_name, figsize = args.figsize, chrlist = chrlist)
#    fig.savefig( out_name, dpi = 300, format = args.format)
#    
#    fig, axs = plot_trans(qtls_df, y_name, x_name, figsize = args.figsize, chrlist = chrlist)
#    fig.savefig( resdir + y_name + chrs_suffix + '.' + args.format, dpi = 300, format = args.format)
    ##################################################################
    blacklist = None
    "PLOTTING PER SE"
    fig, axs = scatter_interactions(qtls_df, x_name = x_name, y_name = y_name, 
                chrlist = chrlist,  figsize = args.figsize,
                blacklist = blacklist, legendtitle = '-log10(p-value)',
                zz = ('STATS', z_variable), dot_size_max = 18,
                alpha = args.alpha)
    out_name = resdir + '/qtls_' + chrs_suffix + '_thr%s' % args.threshold + '.' + args.format
    fig.savefig(out_name, dpi = args.dpi, format = args.format)

    print('see :' , out_name, sep = '\t', file = sys.stderr)

if blacklist:
    print('black list:')   
    for _, x in blacklist[blacklist['chr'] == 'chr1'].iterrows():
        print(x['start'])
