



def indata_to_hdf5( xbed, sep = "\t",
    indexcols =[0],
    chunksize=10000,
    group = "xdf"
    )

    # create a store
    with pd.HDFStore('tempstore.h5') as store:
        for nn, chunk in enumerate(pd.read_table(xbed, chunksize=chunksize, sep = sep, index_col= indexcols)):
            store.append(group, chunk, format = "table", min_itemsize={chunk.index.name if len(indexcols)>1 else "index": 32 })
            print("chunk #" , nn, file = sys.stderr)
    return


hits = []
with pd.HDFStore('tempstore.h5') as store:
    print(store.groups)
    xchunksize = 1000
    ychunksize = 2000
    for xn, xchunk in enumerate(store.select('xdf', chunksize=xchunksize)):
        for yn, ychunk in enumerate(store.select('ydf', chunksize=ychunksize)):

            Nindividuals = xchunk.shape[1]
            xchunk  = (xchunk.T/ xchunk.std(axis = 1) ).T
            ychunk  = (ychunk.T/ ychunk.std(axis = 1) ).T

            rthr= 0.8
            R = pd.DataFrame( 1/Nindividuals * xchunk.as_matrix().dot( ychunk.as_matrix().T ), index = xchunk.index, columns=ychunk.index )

            for nn, vv in (abs(R)>rthr).iterrows():
                nnzs =  R.columns.get_values()[vv.nonzero()[0]]
                hits.extend(zip( [nn] * nnzs.shape[0], nnzs.tolist(), R.loc[nn].iloc[ vv.tolist()] ))
                #print(nn, vv.nonzero()vv.nonzero())
                pass
            print("iteration: ",xn, yn)
            if xn*yn>16:
                break

