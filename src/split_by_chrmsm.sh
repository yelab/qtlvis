#!/bin/bash

input=$1 # "cd4.atac.unique.peak.qar.matrix.eqtl.txt"
output_prefix="indatachr/atacs.meta."
output_suffix=".matrix.eqtl.txt"

n="X"
head -n1 $input | cat -  $input | grep "^chr${n}:" $input > "${output_prefix}chr$n${output_suffix}"
n="Y"
head -n1 $input | cat - $input | grep "^chr${n}:" $input > "${output_prefix}chr$n${output_suffix}"

END=22
for n in $(seq 1 $END)
do
     head -n1 $input | cat - $input | grep "^chr${n}:" $input > "${output_prefix}chr$n${output_suffix}"
done

