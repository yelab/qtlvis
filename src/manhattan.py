import numpy as np
import pandas as pd
from natsort import natsorted
from itertools import cycle

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec


class Manhattan():
    def __init__(self, data,
                 tickspacing = 5e7,
                 xrotation=-45,
                 vlinecolor = [.8]*3,
                 vlinewidth = 1,
                 chromosome = "chromosome",
                 position = "position",
                 pvalue = "p-value",
                 neglog_pvalue = "neglog p-value",
                 colors = [],
                 figsize = None,
                 edgecolor = 'none',
                 threshold0 = {"level": 1e8, "color": 'r', 'ls':'-'},
                 fig = None,
                 axs = None,
                 **kwargs):

        self.tickspacing = tickspacing

        if "vlw" in kwargs:
            vlinewidth = kwargs.pop("vlw")

        # collect threshold dictionaries
        thresholds = {}
        if type(threshold0) is dict:
            thresholds[0] = threshold0
        for kw in list(kwargs.keys()):
            if kw.startswith("threshold") and type(kwargs[kw]) is dict:
                print(kw, kwargs.keys())
                thresholds[kw.rstrip("threshold")] = kwargs.pop(kw)

        # get chromosome lengths [in bp] and create axes
        max_pos = type(self)._max_pos_(data)

        # create axes
        if axs is None:
            self.fig, self.gs, self.axs = type(self)._manhattan_axes_(
                max_pos,
                tickspacing = tickspacing,
                xrotation=xrotation,
                vlinecolor=vlinecolor,
                vlinewidth=vlinewidth,
                figsize=figsize,
                fig = fig,
            )
        else:
            self.axs = axs
            self.fig = axs[0].figure

        # plot the data within the created axes
        type(self)._plot_data_(data, self.axs,
                               chromosome = chromosome,
                               position = position,
                               pvalue = pvalue,
                               neglog_pvalue = neglog_pvalue,
                               colors=colors,
                               edgecolor=edgecolor,
                               **kwargs)

        for thr, thrdict in thresholds.items():
            try:
                level = thrdict.pop('level')
            except:
                print(thrdict)# , file=sys.stderr)
                continue
            for ax in self.axs:
                ax.axhline(-plt.np.log10(level), **thrdict)

        # polish the x-axes ranges:
        self._polish_xaxis_()


    @staticmethod
    def _plot_data_(data, axs, colors=[],
                    edgecolor='none',
                    chromosome = "chromosome",
                    position = "position",
                    pvalue = "p-value",
                    neglog_pvalue = "neglog p-value",
                    **kwargs):
        if len(axs) == 0:
            raise ValueError("the axes array is empty!")
        # pick the color cycle
        if colors is None or len(colors)==0:
            color_cycle = (cc["color"] for cc in axs[0]._get_lines.prop_cycler)
        else:
            color_cycle = cycle(colors)
        # order chromosomes in natural order
        data = data.set_index(chromosome).loc[natsorted(data[chromosome].unique())].reset_index()
        for (chrm, gg), cc in zip(data.groupby(chromosome, sort=False), color_cycle):
            axs[chrm].autoscale(enable=True, axis="y")
            axs[chrm].scatter(gg[position],
                                   gg[pvalue].map(lambda x: -np.log10(x)),
                                   edgecolor=edgecolor, c=cc, **kwargs)
        # set vertical limits
        ymax = axs[0].get_ylim()[-1]
        ymax = int(min(1.05*np.ceil(ymax), round(ymax)))
        ylims = np.r_[-0.05, 1.05]*ymax
        axs[0].set_ylim(ylims)
        yticks = np.arange(0, ylims[-1])

        for ax in [axs[0], axs[-1]]:
            ax.set_yticks(yticks)
            ax.set_yticklabels(yticks.astype(int))


    def _polish_xaxis_(self):
        if not hasattr(self,"gs"):
            return
        ranges = []
        for chrm, ax in self.axs.items():
            xlim = list(ax.get_xlim())
            xlim[0] = -0.05*self.tickspacing
            ax.set_xlim(xlim)
            ranges.append(xlim[1] - xlim[0])
        self.gs.set_width_ratios(ranges)


    @staticmethod
    def _max_pos_(data):
        max_pos = data.groupby("chromosome").position.max()
        max_pos = max_pos.reindex(index=natsorted(max_pos.index))
        return max_pos


    @staticmethod
    def _manhattan_axes_(max_pos, tickspacing = 2.5e7,
                         xrotation=-45,
                         vlinecolor = [.8]*3,
                         vlinewidth = None,
                         fig=None,
                         figsize=None,
                         **kwargs
                         ):
        num_axes = len(max_pos)
        if fig is None:
            fig = plt.figure(figsize=figsize)
        gs = gridspec.GridSpec(1, num_axes, hspace=0.0, wspace=0.0)
        gs.set_width_ratios(max_pos.tolist())

        axs = []
        prevax = None
        for nn, (chrm, gg) in enumerate(zip(natsorted(max_pos.index), range(gs.get_geometry()[1]))):
            ax = fig.add_subplot(gs[0, gg], sharey = prevax)
            axs.append((chrm, ax))
            prevax = ax
            ax.set_xticklabels([])
            if nn>0:
                ax.set_yticklabels([])
                ax.spines['left'].set_color(vlinecolor)
                ax.spines['left'].set_linewidth(vlinewidth)
                if nn < num_axes:
                    ax.tick_params(axis='y', colors='none')
            if nn == num_axes:
                ax.spines['right'].set_color('none')
            # display only a subset of x-labels
            if (nn < 10) | (nn%2-1):
                ax.set_xlabel(chrm, rotation = -xrotation)
            xticks = plt.np.arange(0, tickspacing*(2+max_pos.loc[chrm]//tickspacing), tickspacing)
        #             xticks[-1] = max_pos[chrm]
            ax.set_xticks(xticks)
        return fig, gs, pd.DataFrame(axs, columns=["chromosomes","axes"]).set_index("chromosomes")["axes"]

