#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 23 12:32:05 2015

@author: dlituiev
"""
import sys, os
from warnings import warn
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
sys.path.append('../src')
from qqplot_empirical import qqplot_emp, fdr
# import pdscatter as pdsc
from read_qtls_onefile import *
###############################################################################
def breaklist(lst, distinct_start='permuted.', common_end= ".matrix.eqtl.h5"):
    prmtd = filter(lambda x: (x.endswith(common_end) and x.startswith(distinct_start)) , lst)
    
    for x in lst:
        if x.endswith(common_end) and not x.startswith(distinct_start):
            orig = x
            break
    return orig, prmtd
    
def pltr(func):
    def wrapper(self, *args, **kwargs):
        fig = func(self, *args, **kwargs)
        fig.tight_layout()        
        figname = func.__name__
        # fig.suptitle
        fpath = self.outdir + '/' + '.'.join([ figname, self.cistrans,  self.fmt])
        fig.savefig( fpath, dpi = self.dpi, format = self.fmt)
        print("figure saved to:", fpath, sep = "\t", file = sys.stderr )
        return
    return wrapper

class permutation_analysis():
    def __init__(self, p_intact, p_permuted, npermutations,
                 cistrans, outdir = './', fmt = 'png', dpi = 300):
        self.p_intact = np.array(p_intact)
        self.p_permuted = np.array(p_permuted)
        self.p_intact.sort()
        self.p_permuted.sort()
        self.npv_perm = len(self.p_permuted)
        self.nperm = npermutations
        self.fmt = fmt
        self.dpi = dpi
        self.outdir = outdir
        self.cistrans = cistrans
        
    @property
    def qval(self):
        if not hasattr(self, '_qval_'):
             self._qval_ = fdr(self.p_intact, self.p_permuted, self.nperm)
             #list(map(self.fdr, self.p_intact) )
        return self._qval_
    
    @property
    def neglogp_intact(self):
        return -np.log10(self.p_intact)
    
    @property
    def neglogp_permuted(self):
        return -np.log10(self.p_permuted)
    
    @pltr
    def qqplot(self):
        print("plotting QQ-plot", file = sys.stderr)        
        fig, _ = qqplot_emp(self.neglogp_intact, self.neglogp_permuted, figsize = 4,
                        xlab = "null -log10(p-value)", ylab = "QTL -log10(p-value)" )
        return fig
    @pltr
    def qvalnum_plot(self):        
        ecdf_cis = (np.arange(len(self.p_intact))) # np.flipud
        fig, ax = plt.subplots(1)
        fdrs = np.array(self.qval)
        fdrs[fdrs>1] = np.inf
        ax.plot(ecdf_cis,  fdrs)
        plt.xlabel("number of QTLs")
        plt.ylabel("q-value")
        plt.grid(which= 'major')
        #ax.set_ylim([0,1])
        ml = MultipleLocator(50)
        ax.xaxis.set_minor_locator(ml)
        return fig
    
    @pltr
    def pval_hist(self):
        print("plotting p-value histograms", file = sys.stderr)
        
        fig, ax = plt.subplots(1)
        weights = lambda myarray: np.ones_like(myarray)/float(len(myarray))

        ax.hist(self.neglogp_intact, bins=50, weights = weights(self.neglogp_intact))
        ax.hist(self.neglogp_permuted, bins= 50, color = (1,0,0,0.4) , weights = weights(self.neglogp_permuted))
        plt.xlabel('-log p-value')
        plt.ylabel('fraction')
        plt.title(self.cistrans + ' red: permuted')
        fig.tight_layout()
        return fig

    def fdr(self, z):
        return FDR(z, self.p_intact, self.p_permuted,  self.nperm )
    
    @pltr
    def pqplot(self, maxpoints = 200):
        print("plotting FDR", file = sys.stderr)
        # xx = 10**np.arange(-12, -3, 1/8)
        n = round(self.p_intact.shape[0]/maxpoints) if self.p_intact.shape[0] > maxpoints else 1
        xx = self.p_intact[::n] 
        print("%u data points" % len(xx), file = sys.stderr)
        q = np.array(self.qval(xx))
        #ylims = [0, 0.06]
        # q[q>1] = np.inf
        fig, ax = plt.subplots(1,1, figsize = (5,3))
        plt.plot(-np.log10(xx), q)
        xlims = -np.log10([max(xx), min(xx)] )
        if max(q) >1:
            plt.plot( xlims, [1,1], '--',
                     color = (0,0,0,0.7), zorder = -1)
        plt.xlim(xlims)
        #plt.ylim(ylims)
        plt.xlabel('-log p-value')
        plt.grid(which= 'major')
        plt.ylabel('FDR')
        plt.title(self.cistrans)
        return fig
    
    def plotall(self):
        self.pval_hist()
        self.qqplot()
        self.pqplot()
        self.qvalnum_plot()
###############################################################################
def read_meq_h5(datadir):
    lst=os.listdir(datadir)
    assert ('h5' in [x.split('.')[-1] for x in lst]), "\n".join(["no hdf5 `h5` files found in the directory"]+ lst)
    
    orig, prmtd = breaklist(lst, distinct_start= args.permutation_prefix, common_end= ".matrix.eqtl.h5")
    
    with pd.HDFStore('/'.join([datadir, orig]),  mode='r') as store:
        # print(store)
        p_cis = store.select('/me/cis/eqtls')
        p_trans = store.select('/me/trans/eqtls')   
        
        p_cis["cis"] = True
        p_trans["cis"] = False
        intact_all = pd.concat([p_cis, p_trans], axis = 0)
        
    p_cis_perm = []
    p_trans_perm = []
    npermutations = 0
    for pf in prmtd:
        with pd.HDFStore('/'.join([datadir, pf]),  mode='r') as store:
            npermutations += 1
            print("sample %2u" % npermutations, file = sys.stderr)
            p_cis_p = store.select('/me/cis/eqtls')
            p_trans_p = store.select('/me/trans/eqtls')
            p_cis_perm.extend( p_cis_p["pvalue"].tolist() )
            p_trans_perm.extend(  p_trans_p["pvalue"].tolist() )
    try:
        del p_trans_p
        del p_cis_p
    except Exception as err:
        warn("no permutations found!")
#        print( err, file = sys.stderr )
    perm_all = {"cis":p_cis_perm, "trans": p_trans_perm}
    return intact_all, perm_all, npermutations

###############################################################################
def pqplot(intact_all):
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(1)
    ax.scatter( np.log10(intact_all['pvalue'][cis_inds]), np.log10(intact_all['qvalue'][cis_inds]), edgecolor = '', label = 'cis')
    ax.scatter( np.log10(intact_all['pvalue'][~cis_inds]), np.log10(intact_all['qvalue'][~cis_inds]), c='r', edgecolor = '', label = 'trans')
    plt.xlabel('-log10[p-value]')
    plt.ylabel('-log10[q-value]')
    plt.legend(loc = 'lower right')

###############################################################################
if __name__ == '__main__':
    ###############################################################################
    import argparse
    parser = argparse.ArgumentParser('''reads and reformats binary hdf5 files from matrix eQTL 
    (a file for all chromosomes + several permutation files )
    ''')

    parser.add_argument("indir", nargs='?', type=str,
            default='/Users/dlituiev/repos/qtl_atac_rna/gt_atac/',
            help="output file (.csv); for stdout type '-' ")

    parser.add_argument("-a", "--xmeta",  type=str, default = "",
                        help="metafile for x ('genotype') variable")

    parser.add_argument("-b", "--ymeta",  type=str, default = "",
                        help="metafile for x ('genotype') variable")
    
    parser.add_argument("-x",  type=str, default= "genotype",
                        help="name of x variable")

    parser.add_argument("-y",  type=str, default= "expression",
                        help="name of y variable")
                        
    parser.add_argument("-f", "--format", type= str, default= 'png',
                        help="graphics format (png, jpg, eps, pdf)")

    parser.add_argument("-s", "--figsize", type= float, default= 20,
                        help="figure size (side) in inches")

    parser.add_argument("-t", "--threshold", type= float, default= 7,
                        help="-log_10 p-value threshold")

    parser.add_argument("-p", "--permutation_prefix",  type=str, default= 'permuted.',
                        help="a prefix of the permutated h5 files")

    parser.add_argument("-d", "--dpi", type= int, default= 300,
                        help="resolution [dpi]")

    args = parser.parse_args()
    ###############################################################################
    #resdir = '/Users/dlituiev/repos/qtl_atac_rna/pergenegrouped/results/cov_rnalog_bins1e2_within1e3_cisw1e5'
    #resdir = '/Users/dlituiev/repos/qtl_atac_rna/results/newnorm/rnapc10'

    resdir = args.indir
    print('working on: ' , resdir, file = sys.stderr)

    lst=os.listdir(resdir)
    if 'h5' in lst:
        datadir = resdir + '/h5/'
    else:
        datadir = resdir
   
    intact_all, perm_all, npermutations = read_meq_h5(datadir)

    cis_inds = intact_all['cis']
    if ( len(perm_all["cis"]) or len(perm_all["trans"]) ):

        hist_cis = permutation_analysis(intact_all[cis_inds]['pvalue'], perm_all['cis'], 
                               npermutations, outdir = resdir, cistrans= 'cis')

        hist_trans = permutation_analysis(intact_all[~cis_inds]['pvalue'], perm_all['trans'], 
                               npermutations, outdir = resdir, cistrans= 'trans')

        # intact_all["qvalue"] = 1
        intact_all.loc[cis_inds,"qvalue"] =  hist_cis.qval
        intact_all.loc[~cis_inds, "qvalue"] =  hist_trans.qval
    
        intact_all.iloc[0]
   
    descr = pd.DataFrame({'data': [args.x, args.y]}, index = ['x', 'y'])
    r_meta =  rna_meta(args.ymeta)
    g_meta =  gt_meta(args.xmeta, window = 100)
    qtls_df = get_xy( intact_all, g_meta, r_meta,
                        x_name = descr.loc['x'][0],
                        y_name = descr.loc['y'][0]) 

    print(' qtls_df.shape ')
    print( qtls_df.shape )
    print( 'sum( ("chr9:79318015-79320015" == qtls_df[args.x]["name"] ) & ( "NUDT22"  == qtls_df[args.y]["name"]) )'  )
    print( sum( ("chr9:79318015-79320015" == qtls_df[args.x]["name"] ) & ( "NUDT22"  == qtls_df[args.y]["name"]) )  )


    dlen =  len(intact_all) - len(qtls_df)
    if dlen >0:
        warn( "%u rows are lost; %u left" % ( dlen, len(qtls_df) ) )

        print(intact_all.columns)
        pairsbefore = set( zip( intact_all["SNP"].tolist(), intact_all["gene"].tolist() ) )
        pairsafter = set( zip( qtls_df[args.x ][ "name" ] , qtls_df[args.y ][ "name" ] ) )
        pairdiff = pairsbefore - pairsafter


        print( "lost:", file = sys.stderr)
        for x in list(pairdiff)[:20]:
            print( x, file = sys.stderr)
        print( "...", file = sys.stderr)

#        print( "kept:", file = sys.stderr)
#        for x in list(pairsafter)[:20]:
#            print( x, file = sys.stderr)
#        print( "...", file = sys.stderr)


    if not qtls_df['STATS','cis'].any():
        print('no cis QTLS !!!', file=sys.stderr)
    else:
        print(  '%u cis' % qtls_df['STATS','cis'].sum(), file = sys.stderr )


    "save df"
    with pd.HDFStore(resdir + '/qtls.h5',  mode='w') as store:
        print("saving to a HDF5", file = sys.stderr)
        store.append('qtls_df', qtls_df.convert_objects(), format='table')
        store.append('description', descr, format='table')         
    
    if ( len(perm_all["cis"]) or len(perm_all["trans"]) ):
        hist_cis.qqplot()
        hist_trans.qqplot()
#    hist_cis.plotall()
#    hist_trans.plotall()
