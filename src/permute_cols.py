#!/usr/bin/python
from __future__ import print_function
import sys
import numpy as np
from runshell import *
# print 'Number of arguments:', len(sys.argv), 'arguments.'

"empty cells are skipped by awk if not set: FS = \t"

infile = sys.argv[1]
"read the headers"
with open(infile) as f:
    line = f.readline().rstrip('\n')
    samples = line.split('\t')[1:]
    print('number of individuals: %u' % ( len(samples) ) , file = sys.stderr )

print('individual labels before:', file = sys.stderr)
print(samples, file = sys.stderr)

N = len(samples)

clmn_num = np.arange(0,N)
np.random.shuffle(clmn_num)

outfile = infile+".permuted"
clmnlist = ','.join(['$%u' % (x + 1) for x in clmn_num] )
cmd = """head -n1 %(infile)s > %(outfile)s;
awk 'BEGIN{ FS = "\\t"; OFS="\\t"} NR>1{print $1,%(clmns)s}' %(infile)s >> %(outfile)s""" % \
    {"clmns": clmnlist, "infile": infile, "outfile": outfile }
print("running command:", file = sys.stderr)
print(cmd, file = sys.stderr)
out = runcmd(cmd)
print("returned[stderr]: ", out[1], file = sys.stderr)
print("output written to : %s" % outfile, file = sys.stderr)

