# -*- coding: utf-8 -*-
"""
Created on Fri Jul 24 10:35:59 2015

@author: dlituiev
"""

# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

rna_file = "expressions.genes.0.matrix.eqtl.txt"
rna_df = pd.read_csv(rna_file, sep = '\t', index_col = 0).transpose()

from sklearn.decomposition import PCA
# from sklearn.decomposition import RandomizedPCA as PCA

def pca(self, n_components = None):
    pca_ = PCA(n_components= n_components).fit(self)
    pca_.components_
    weights_ = pca_.transform(self)
    cols = [ 'PC%u' % (x+1) for x in range(pca_.components_.shape[0]) ]
    weights_df = pd.DataFrame(weights_, index = self.index, columns = cols)
    return weights_df, pca_

trnsf, pca_ = pca(rna_df, 4)

import pdscatter as pdsc
pdsc.scatter_df(trnsf, 'PC1', 'PC2', 'PC3')

cov_file = 'covariates/basic.covariates.tab.matched'
covariates = pd.read_csv(cov_file, sep = '\t', index_col = 0).transpose()

trnsf['batch'] = covariates['Batch']
trnsf['sex'] = covariates['Sex']

valid = ~trnsf.isnull().any(axis = 1)
pdsc.scatter_df(trnsf[valid], 'PC1', 'PC2', 'batch')

selected = (trnsf['PC1'] >0) & (trnsf['PC2'] <20)


rna_df['STK10'].values.shape
covariates['Sex'].values.shape
covariates.index
rna_df.loc[covariates.index].shape


plt.figure(figsize=(30, 30))
out = rna_df.transpose().ix[:,:16].hist(bins = 100, cumulative = True, edgecolor = 'b')

for x in out.ravel()[:12]:
    x.xaxis.set_ticklabels('')

plt.savefig('expr_cdfs.pdf')
plt.show()

"coverage data"
map_info_f = 'covariates/coverage/old.qc.tab'
cov_df = pd.read_csv(map_info_f, engine = 'python',
                     sep = ' ')# , usecols = ['Sample', 'Mapped Unique'])

# rna_df[:100].iloc[:5].hist(cumulative = True)

# import statsmodels.api as sm
#
#est = sm.OLS(covariates['Sex'].values, rna_df.loc[covariates.index].values)
#estout = est.fit()
#
#plt.hist(estout.params, bins = 40)
#plt.show()
#estout.summary()

# from sklearn.linear_model import LinearRegression
#lr = LinearRegression()
#lr.fit( rna_df.values, covariates['Sex'].values)
#
#LinearRegression(copy_X=True, fit_intercept=True, normalize=False)
#lr.coef_
#lr.intercept_
