# toolbox for postprocessing of output of matrixEQTL module and related genomic 

## PRE-FORMATTING (depricated, use combined step below)
`matrix.eqtl.txt -> h5`

to load standard text output from `matrixEQTL` use following scripts (need tweeking):
`read_qtls.py` (separate chromosomes)
`read_qtls_onefile.py` (separate chromosomes)

## PRE-FORMATTING & DESCRIPTIVE STATISTICS

`matrixEQTL h5  + x-meta + y-meta -> annotated h5 table + QQ-plot + histograms`

use `anls_perm.py` on a directory with output from `matrixEQTL`  
with `h5` output mode (one file for all chromosome so far) to load the data.

It will output empirical QQ-plot and other histograms and metrics.


## PLOTTING INTERACTION MATRIX

`annotated h5 table -> interaction plot`

run `plot_interaction.py` on the directory with data
