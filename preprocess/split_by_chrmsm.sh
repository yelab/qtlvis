#!/bin/bash

echo "./split_by_chrmsm.sh  [input] [output prefix] [output suffix {opt}] " >&2

meta='/ye/netapp/jimmie.ye/cd4.both/best.atac.expressions.meta.matrix.eqtl.txt'
input=$1 # "cd4.atac.unique.peak.qar.matrix.eqtl.txt"

if [ -z $2 ]
then 
    output_prefix=$1
else
    output_prefix=$2
fi

output_suffix="$3"

contigs=`awk 'BEGIN{FS="[:\t]"} (NR>1){print $1}' $input | uniq`
for cc in ${contigs[@]}
do
    echo $cc >&2
    head -n1 $input > ${output_prefix}.$cc
done

echo "splitting" >&2
awk -v pref=$output_prefix 'BEGIN { FS=":" } (NF>1){ print >> pref"."$1; close(pref"."$1) }' $input


exit 0
##############################
END=22
for n in  `seq 1 $END` X Y
do
     ouput_file="${output_prefix}.chr$n${output_suffix}"
     echo $ouput_file >&2
     (head -n1 "$input" &  grep "^chr${n}:" $input) | cat > $ouput_file || exit 1
done

