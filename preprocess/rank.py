#!/usr/bin/env python3
from __future__ import print_function
import scipy.stats as st
import sys

from contextlib import contextmanager
@contextmanager
def smart_open(filename=None, mode = 'w'):
    if filename and filename != '-':
        fh = open(filename, mode)
    elif mode == 'w':
        fh = sys.stdout
    elif mode == 'r':
        fh = sys.stdin
    else:
        print('unknown file access mode: ', mode, file = sys.stderr)

    try:
        yield fh
    finally:
        if fh is not sys.stdout:
            fh.close()
##########################################

infile = sys.argv[1]
outfile= infile.replace('.txt', '') + '.rank'
if len(sys.argv)>2:
   outfile = sys.argv[2]

sep = '\t' 

with open(infile, 'r') as fi, smart_open(outfile, 'w') as fo:
    header = fi.readline().rstrip('\n')
    print(header, file = fo)
    for line in fi:
        cells = line.split(sep)
        vals = [float(x) for x in cells[1:] ]
        ranks = st.rankdata(vals)
        print( cells[0], *ranks, sep=sep, file = fo)
        
print("see :\t", outfile , file = sys.stderr)
