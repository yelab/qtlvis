#!/usr/bin/env python3
from __future__ import print_function
import sys, logging
import numpy as np
import pandas as pd
import math

"""
translated from edgeR package by Robinson and Oshlack, 2010
"""
from tmm_rpy import  cpm
from tmm_rpy import  tmm as rtmm
from qnorm import normalize_quantiles_robust as rqnorm
from tmm_normalize import  tmm, calcNormFactorsTMM, get_ref_col# , calcFactorQuantile
   
from get_pcs import get_pcs
from orthogonalize import orthogonalize, prune_low_var

from subprocess import check_output

def wc(filename):
    return int(check_output(["wc", "-l", filename]).split()[0])
##################################################################
if __name__ == "__main__":

    #####################################################
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", 
                        help="input file (.tab-separated ) with gene annotation")

    parser.add_argument("outFile", nargs='?', type=str, default=r'',
                        help="output file (.csv); for stdout type '-' ")
                        
    parser.add_argument("-c", "--n_ind_cols", default=1, type = int,  help = "number of index columns" )

    parser.add_argument("-p", "--npcs", default=1, type = int,  help = "number of principal components to residualize against" )

    parser.add_argument("-t", "--varthr", default=1e-6, type = float,  help = "lower threshold on variance; rows below threshold are removed" )

    parser.add_argument('-d', "--drop_all_zero_rows", action='store_true', default=False)

    parser.add_argument('-b', '--blacklist', nargs='+', type= str, default = [])

    parser.add_argument('-L', "--log", action='store_true' , dest = "log", default=True)
    parser.add_argument('-l', "--lin", action='store_false', dest = "log", default=True)
    
    parser.add_argument('-q', "--quantile",   action='store_true' , dest = "quantile", default=False)
    parser.add_argument('-Q', "--noquantile", action='store_false', dest = "quantile", default=False)


    args = parser.parse_args()
    #####################################################
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    index_col = list(range(args.n_ind_cols))

    nffile = args.infile + ".norm_factors.tab"

    suffix = ["tmm"]
    if args.quantile:
        suffix.append("q")
    if args.log:
        suffix.append( "log" )
    if args.npcs>0:
        suffix.append( "orth%upcs" % args.npcs )

    outfile = args.infile + "." + "_".join( suffix )

    df = pd.read_table( args.infile , index_col = index_col, nrows = 3)
    if args.drop_all_zero_rows:
        df.dropna( axis = 0, inplace = True )

    cols = df.columns.tolist()
    indexcols = list(df.index.names)
    ncol = len(cols)
    nrow = wc(args.infile)

    size = nrow * ncol

    if size < 1e8:
        df = pd.read_table( args.infile , index_col = index_col)
        if len(args.blacklist) > 0:
            from natsort import natsorted
            cols = natsorted(list(set(df.columns) - set(args.blacklist)) )
            df = df[cols]
            # df = df[ list( set(df.columns) - set(args.blacklist) ) ]
            logging.info( "removed blacklisted columns: %s" % "\t".join(args.blacklist) )

        if df.index.names:
            logging.debug( "index columns:\t" + "\t".join( [ "%s" % x for x  in  df.index.names] ), )

        "Normalization per se"
        dfnorm = rtmm( prune_low_var(df, thr = args.varthr), log = False)
        if args.quantile:
            dfnorm = rqnorm( dfnorm )

        print(cpm( dfnorm , log = args.log ).head())
        dfnorm = cpm( dfnorm , log = args.log ).dropna(0)

        if args.npcs>0:
            df_pcs, df_scores, pca_ = get_pcs(dfnorm.T, n_components= args.npcs)
            dfnorm = orthogonalize(dfnorm,  df_pcs.T )
            dfnorm = prune_low_var(dfnorm, thr = 1e-6)
            logging.info("subtracted %u PCs" % args.npcs )

        dfnorm.to_csv( outfile , sep = "\t")
    else:
        logging.info("work-around for big tables")
        df = pd.read_table( args.infile , index_col = index_col, nrows = int(1e6/ncol) )
        logging.debug( "index columns" , list(df.index.names) , )
        refcolnum = get_ref_col( df, p = 0.75)
        refcolname = cols[refcolnum]
        refcol = pd.read_table( args.infile , index_col = None, usecols =  [refcolname] )

        coldict = {}
        nflist = []
        for cc in cols:
            logging.debug("column  %s" % cc ,)
            othcol =  pd.read_table( args.infile , index_col = None, usecols = [cc] )
            df =  pd.concat([refcol, othcol ], axis = 1)
            norm_fact = calcNormFactorsTMM( df , refcol = 0)
            lib_size = othcol.iloc[:,0].sum()
            nflist.append( (cc, norm_fact, lib_size ) )

            #coldict[cc] = tmm(othcol, norm_fact[-1])
        #dfnorm = pd.concat( coldict , axis = 1)

        nfser = pd.DataFrame( nflist, columns = ["sample", "norm_factor", "lib_size",]).set_index("sample").iloc[:,0]
        nfser.to_csv(nffile, sep = "\t")
        print( "see", nffile, sep = "\t")

    print( "see", outfile, sep = "\t")
