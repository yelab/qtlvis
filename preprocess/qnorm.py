import numpy as np
import pandas as pd
from rpy2.robjects import FloatVector
from rpy2.robjects import Matrix
from rpy2 import robjects
import rpy2.robjects.numpy2ri
rpy2.robjects.numpy2ri.activate()
rmatrix = robjects.r('as.matrix')

#####################################################################
# R package names
packnames = ('preprocessCore',)

# import rpy2's package module
import rpy2.robjects.packages as rpackages

if all(rpackages.isinstalled(x) for x in packnames):
    have_tutorial_packages = True
else:
    have_tutorial_packages = False
    if not have_tutorial_packages:
        # import R's utility package
        utils = rpackages.importr('utils')
        # select a mirror for R packages
        utils.chooseCRANmirror(ind=1) # select the first mirror in the list
#We are now ready to install packages using R's own function install.package:
if not have_tutorial_packages:
    # R vector of strings
    from rpy2.robjects.vectors import StrVector
    # file
    packnames_to_install = [x for x in packnames if not rpackages.isinstalled(x)]
    if len(packnames_to_install) > 0:
        utils.install_packages(StrVector(packnames_to_install))
        
fun = "preprocessCore::normalize.quantiles.robust"
_normalize_quantiles_robust = robjects.r(fun )

def normalize_quantiles_robust(df):
    if type(df) == pd.DataFrame:
        tmp = _normalize_quantiles_robust(df.astype(float).as_matrix())
        return pd.DataFrame(pd.np.array(tmp), index = df.index, columns= df.columns)
    else:
        return np.array(_normalize_quantiles_robust(df))

