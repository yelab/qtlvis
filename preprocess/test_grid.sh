#!/bin/bash

A=$1
B=$2
N=$(($A * $B ))

echo -e "A: $A\t B: $B" >&2

for n in $(seq 0 $(($N - 1)) )
do
    echo -e  "a:  $(( 1 +  n % $A )) \t b: $(( 1 + n / A ))"
done

