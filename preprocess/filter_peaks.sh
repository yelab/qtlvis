#!/bin/bash                         #-- what is the language of this shell
#                                  #-- Any line that starts with #$ is an instruction to SGE
#$ -S /bin/bash                     #-- the shell for the job
#$ -o /netapp/home/dlituiev/logs                        #-- output directory (fill in)
#$ -e /netapp/home/dlituiev/logs                      #-- error directory (fill in)
#$ -cwd                            #-- tell the job that it should start in your working directory
#$ -M d.lituiev@gmail.com
#$ -m ae
#$ -r y                            #-- tell the system that if a job crashes, it should be restarted
#$ -j y                            #-- tell the system that the STDERR and STDOUT should be joined
#$ -l mem_free=4G                  #-- submits on nodes with enough free memory (required)
#$ -l arch=linux-x64               #-- SGE resources (CPU type)
#$ -l netapp=1G,scratch=1G         #-- SGE resources (home and scratch disks)
#$ -l h_rt=24:00:00                #-- runtime limit (see above; this requests 24 hours)
#$ -t  1                        #-- remove first '#' to specify the number of
                                  #-- tasks if desired (see Tips section)
##########################################################################

input_dir='/ye/netapp/jimmie.ye/cd4.atac.seq/Atac_seq_02_12_15_05_14_15_hiseq'

head -n1 ${input_dir}/unique.keep.bed > ${input_dir}/header.bed || exit 1
sed '1d' ${input_dir}/unique.keep.bed | bedtools intersect -a - -b /ye/netapp/jimmie.ye/cd4.atac.seq/Atac_seq_14_12_28_14_hiseq/motif.analysis/archive/all_48hr.nuclear.keep.unique.200_peaks.narrowPeak -wa | cat ${input_dir}/header.bed - > ${input_dir}/unique.peak.bed || exit 1
