#!/bin/bash
echo "./select_inds.sh [covariate field]  [covariate value] " >&2

covariates='covariates/basic.covariates.tab.matched'

inds=`./select_inds.R "$2" "$3" "$covariates" `

echo "${inds}" >&2
cut -f${inds} $1 || exit 1

