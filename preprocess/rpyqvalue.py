import pandas as pd
import numpy as np
import rpy2
from rpy2.robjects import FloatVector, IntVector, Matrix
from rpy2.rinterface import FloatSexpVector
from rpy2 import robjects
from rpy2.robjects import pandas2ri
from rpy2.robjects import numpy2ri
#import rpy2.robjects.numpy2ri
numpy2ri.activate()
pandas2ri.activate()

import rpy2.robjects.packages as rpackages
def activate_r_packages(packnames):
    if all(rpackages.isinstalled(x) for x in packnames):
        have_tutorial_packages = True
    else:
        have_tutorial_packages = False
        if not have_tutorial_packages:
            # import R's utility package
            utils = rpackages.importr('utils')
            # select a mirror for R packages
            utils.chooseCRANmirror(ind=1) # select the first mirror in the list
    #We are now ready to install packages using R's own function install.package:
    if not have_tutorial_packages:
        # R vector of strings
        from rpy2.robjects.vectors import StrVector
        # file
        packnames_to_install = [x for x in packnames if not rpackages.isinstalled(x)]
        if len(packnames_to_install) > 0:
            utils.install_packages(StrVector(packnames_to_install))

packnames = ("qvalue",)
activate_r_packages(packnames)
# ntests = df["interaction_lg_pval"]
def cast_from_r(x):
    if type(x) in (FloatVector, IntVector):
        return np.array(x)
    if type(x) is Matrix:
        return np.array(x)
    if type(x) is rpy2.robjects.methods.RS4:
        try:
            return np.array(rmatrix(x))
        except:
            return x

###############################################################
class QValue():
    fun = "qvalue::qvalue"
    _qvalue_ = robjects.r(fun,)

    def __init__(self, pvals, **kwargs):
        if type(pvals) in (np.array, np.ndarray):
            pvals = FloatSexpVector(list(pvals))
            #pvals = rpy2.robjects.DataFrame()
            #pvals = pd.Series(pvals)
            #print(type(pvals))
        _r_output = self._qvalue_(pvals, **kwargs)
        self.__dict__.update(dict((k.replace(".", "_").replace("lambda", "lambda_"), cast_from_r(v)) \
                                  for k,v in _r_output.items()))
        self.pi0 = self.pi0[0]
        if type(pvals) is pd.Series:
            self.qvalues = pd.Series(self.qvalues, index = pvals.index)

    def __call__(self):
        return self.qvalues

    def __repr__(self):
        return repr(type(self)) + """
        pi0 = {pi0}
        number of pvalues = {n}
        """.format(pi0 = self.pi0, n=len(self.qvalues))


def qvalue(pvals, **kwargs):
    q = QValue(pvals)
    return q()

###############################################################
def empPvals(stat, stat0, pool = True):
    """
    # Description
    Calculates p-values from a set of observed test statistics and simulated null test statistics

    # Usage

        empPvals(stat, stat0, pool = TRUE)

    # Arguments

        stat    --    A vector of calculated test statistics.
        stat0   --    A vector or matrix of simulated or data-resampled null test statistics.
        pool    --    If FALSE, stat0 must be a matrix with the number of rows equal to the length of stat. Default is TRUE.

    # Details

    The argument stat must be such that the larger the value is the more deviated (i.e., "more extreme") from the null hypothesis it is. Examples include an F-statistic or the absolute value of a t-statistic. The argument stat0 should be calculated analogously on data that represents observations from the null hypothesis distribution. The p-values are calculated as the proportion of values from stat0 that are greater than or equal to that from stat. If pool=TRUE is selected, then all of stat0 is used in calculating the p-value for a given entry of stat. If pool=FALSE, then it is assumed that stat0 is a matrix, where stat0[i,] is used to calculate the p-value for stat[i]. The function empPvals calculates "pooled" p-values faster than using a for-loop.

    See page 18 of the Supporting Information in Storey et al. (2005) PNAS (http://www.pnas.org/content/suppl/2005/08/26/0504609102.DC1/04609SuppAppendix.pdf) for an explanation as to why calculating p-values from pooled empirical null statistics and then estimating FDR on these p-values is equivalent to directly thresholding the test statistics themselves and utilizing an analogous FDR estimator.

    # Returns

    A vector of p-values calculated as described above.

    # Author(s):
    John D. Storey

    #References:

    Storey JD and Tibshirani R. (2003) Statistical significance for genome-wide experiments. Proceedings of the National Academy of Sciences, 100: 9440-9445. 
    http://www.pnas.org/content/100/16/9440.full

    Storey JD, Xiao W, Leek JT, Tompkins RG, Davis RW. (2005) Significance analysis of time course microarray experiments. Proceedings of the National Academy of Sciences, 102 (36), 12837-12842. 
    http://www.pnas.org/content/102/36/12837.full.pdf?with-ds=yes


        """
    _empPvals_ = robjects.r( "qvalue::empPvals" )
    _r_output = _empPvals_(stat, stat0, pool)
    if type(stat) in (pd.Series, pd.DataFrame):
        return pd.Series([vv for kk, vv in _r_output.items()], index = stat.index)
    else:
        return np.array(_r_output)
