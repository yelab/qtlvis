#!/bin/bash
min_r=5
max_r=10

min_a=2
max_a=5

for aa in $(seq $min_a $max_a)
do
    for rr in $(seq $min_r $max_r)
    do
        echo  -e "a: ${aa}\t r: $rr"
        ./fuse_covariates.sh $aa $rr
    done
done
