import numpy as np
import pandas as pd
import sys
from scipy.stats import distributions
import seaborn as sns
import matplotlib.pyplot as plt
import scipy.cluster.hierarchy as hierarchy
from scipy.spatial import distance
from orthogonalize import prune_low_var, standardize
import statsmodels.api as sm

def lmm_repeatability(Y, individuals):
    model = sm.MixedLM( Y, exog=np.ones_like(Y), groups=individuals)
    result = model.fit()
    err_var = result.scale
    indiv_var = result.cov_re.iloc[0,0]
    tot_var = indiv_var + err_var
    fract_indiv = indiv_var/tot_var
    likev = result.profile_re(0, dist_low=0, dist_high=0, num_low=0, num_high=0)
    return (fract_indiv, result.llf)


def tstat_r(r, n):
    return (n-2)*r/np.sqrt(1 - r**2)

def feature_correlation(df1, df2):
    pairwisecorr_orth = (standardize(df1, axis = 1)  * standardize(df2, axis = 1)).sum(1)
    pairwisecorr_orth = pairwisecorr_orth.loc[ ~np.isnan(pairwisecorr_orth)]
    return pairwisecorr_orth

class replicate_analysis():

    def __init__(self, df1, df2, func= None, prefix = "_", suffix = "_",
                 thr = 1e-6, keep_data = False):
        
        df1, df2 = prune_low_var(df1, df2, thr = thr)

        if func is not None:
            df1 = func(df1)
            df2 = func(df2)
        
        self.batchnames = ["%s%u%s" % (prefix, x, suffix) for x in range(1,1+2) ]
        
        #sample_corr = standardize(pd.concat({"rc1": df1_orth, "rc2": df2_orth}, axis = 1).dropna(0), axis = 0).corr()
        
        all_samples =  pd.concat( {self.batchnames[0]: df1, 
                                   self.batchnames[1]: df2},
                            axis = 1).dropna(0)
        self.mean = all_samples.mean(1)
#        self.lmmrep = all_samples.apply(lambda x : lmm_repeatability(x, 
#                                            all_samples.columns.get_level_values(1)),
#                                        axis=1)
#
        all_samples_st = standardize(all_samples, axis = 0)

        if keep_data:
            self.all_samples = all_samples
        del all_samples

        "standardize so that mean(expression in an individual) =  0 etc."
        self.sample_corr = all_samples_st.corr()
        self.row_linkage = hierarchy.linkage(
                    distance.pdist(all_samples_st.T), method='average')
       
        #total > np.percentile(total, 5)
        #self.feature_corr = (standardize(df1, axis = 1)  * standardize(df2, axis = 1)).sum(1)
        self.feature_corr = feature_correlation(df1, df2) #self.feature_corr.loc[ ~np.isnan(self.feature_corr)]

    def plot(self, nodiag = False):
        if nodiag:
            g = sns.clustermap(self.sample_corr - np.eye( self.sample_corr.shape[0]),
                               row_linkage= self.row_linkage, col_linkage = self.row_linkage) 
        else:
            g = sns.clustermap(self.sample_corr , row_linkage= self.row_linkage, 
                               col_linkage = self.row_linkage)  
        plt.setp( g.ax_heatmap.yaxis.get_majorticklabels(), rotation=0 )
        pass
    
    def hist(self, bins=100):
        
        out = self.feature_corr.hist(bins = bins)
        plt.xlim([-1.1,1.1])
        plt.title( "mean R: %.4f" % self.feature_corr.mean() )
        return out
        
    def pvalhist(self):
        nn = df1.shape[1]
        p_vals = distributions.t.sf(np.abs(tstat_r(self.feature_corr[self.feature_corr>0], nn)),
                                    nn-2)
        plt.hist( np.log10(p_vals), bins= 100)
        pass

    @property
    def ranklist(self):
        if not hasattr(self, "_ranklist_"):
            self._ranklist_ = pd.Series(index = self.sample_corr[self.batchnames[0]].columns)
            for ii in self.sample_corr[ self.batchnames[0] ].columns.tolist():
                ds = pd.DataFrame(self.sample_corr.loc[( self.batchnames[0], ii)].\
                                  sort_values(ascending = False))
                self._ranklist_.loc[ii] = ds.index.tolist().index((self.batchnames[1], ii))
            self._ranklist_ = self.ranklist.astype(int)

        return self._ranklist_

    @ranklist.setter 
    def ranklist(self, value):
        if not hasattr(self, "_ranklist_"):
            self._ranklist_ = pd.Series(index = self.sample_corr[self.batchnames[0]].columns)
        setattr(self._ranklist_, value )

    @property
    def rank_metric(self):
        """hyperbolic mean of ranks"""
        return 1/self.ranklist.map(lambda x : 1/x).sum()
    


