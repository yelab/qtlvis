#!/bin/awk -f
BEGIN { FS="[-,:,\t]";
 RS="\n";
 OFMT = "%.0f";
 OFS="\t";
}
{
    printf $1":"$2"-"$3"\t";
    print $1,$2,$3; 
}
