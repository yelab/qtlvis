#!/usr/bin/env python3
import sys, os
import pandas as pd

def _read_df_if_path_(rna, index_col=[0]):
    if type(rna) == str and os.path.exists(rna):
        return pd.read_table(rna, index_col = index_col)
    elif type(rna) is pd.DataFrame:
        return rna 
    else:
        raise IOError("wrong input / file not found:", rna)
 
def gene_to_bed_index(rna, rnameta, drop_gene = True, indname = "gene"):
    rna = _read_df_if_path_(rna)
    rnameta = _read_df_if_path_(rnameta)

    rna_bed = pd.concat( [rna, rnameta], axis = 1).dropna()
    if not drop_gene:
        rna_bed.index.names = [indname]
        rna_bed.reset_index(inplace = True)

    rna_bed = rna_bed.set_index(rnameta.columns.tolist())
    rna_bed = rna_bed.reset_index()
    rna_bed["start"] = rna_bed["start"].astype(int)
    rna_bed["end"] = rna_bed["end"].astype(int)
    try:
        rna_bed.rename(columns = {"chr": "#chr"}, inplace = True)
    except:
        pass
    rna_bed.set_index(["#chr", "start", "end"], inplace = True)
    return rna_bed

def select_genes_on_chr(rna, rnameta, cc, indname = "gene"):
    rnabed =  gene_to_bed_index(rna, rnameta, drop_gene = False, indname = indname).reset_index()
    rnabed = rnabed.loc[rnabed["#chr"] == cc].drop( ["#chr", "start", "end"], axis = 1).set_index(indname)
    return rnabed

if __name__ == "__main__":

    rnafile = sys.argv[1]
    if len(sys.argv > 2):
        rnametafile = sys.argv[2]
    else:
        rnametafile = "rna.genes.meta"

    rnabed = rnafile.\
                replace(".matrix.eqtl.txt", "").\
                replace(".matrix", "").\
                replace(".txt", "").\
                replace(".tab", "") + ".bed"

    rna_bed.to_csv(rnabed, sep = "\t", index = False)

