#!//usr/bin/env python3
import numpy as np
import pandas as pd

def prune_low_var( *args, thr = 1e-6):
    var = 0
    for x in args:
        var = var + x.var(1)
    valid = (var >= thr)
    out = []
    for dfx in args:
        out.append(     dfx[ valid ]    )
    #var = var[var >= thr]
    if len(out) == 1:
        return out[0]
    else:
        return out

def prune_low_var_pair( df1, df2, thr = 1e-6):
    var = (df1.var(1) + df2.var(1))
    df1 = df1[var >= thr]
    df2 = df2[var >= thr]
    return df1, df2

#def standardize(x, axis=1):
#    if axis == 0:
#        tmp = (x - x.mean(axis=axis))/x.std(axis = axis)
#    elif axis == 1:
#        tmp = ((x.T - x.mean(axis = axis)) /x.std(axis = axis) )
#        tmp = (tmp - tmp.mean()).T
#        tmp = tmp.dropna(axis = 0)
#    return tmp

def standardize(x, axis=0, dropna = False):
    if axis == 1:
        x = x.T
    x = (x - x.mean(0))
    if len(x.shape) > 1:
        valid = ~(x.std(0) == 0)
        x = x.T[ valid ].T
#     x = x/np.sqrt((x**2).sum(0))
    x = x/(x.std(0)*np.sqrt(x.shape[0]))
    if dropna and type(x) is pd.DataFrame:
        x = x.dropna(axis = 1)
#     else:
#         x = x[:, ~ np.isnan(x) ]
    if axis == 1:
        x = x.T    
    return x


def fix_covariates(df):
    for kk, rr in df.iterrows():
        if not rr.isnull().any():
            continue
        naflag = rr.isnull()
        print("fixing:\t%s\tin\t%s" % \
            (kk, repr(rr.index[naflag].tolist())) )
        rr[naflag] = rr[~naflag].mean()
        df.loc[kk] = rr
    return df


def orthogonalize(y, x, thr = 1e-10):
    """Residualized  matrix `y` against the second (`x`)
    Input: first matrix must be centered"""
    if not (y.sum(axis=1)<thr).all():
        "matrix `y` is not centred"
        y = (y.T - y.mean(axis = 1)).T

    if not (x.sum(axis=1)<thr).all():
        "matrix `x` is not centred"
        x = (x.T - x.mean(axis = 1)).T

    rowsq1 = (y**2).sum(1)

    if np.any(np.isnan(x)):
        raise ValueError("NAs in X matrix")

    q, r = np.linalg.qr(x.T)
    del r
    if np.isnan(q).any():
        raise ValueError("NAs in Q matrix")
    proj = np.dot(y, q)
    out = y - np.dot(proj, q.T)
    del y, q
    
    rowsq2 = (out**2).sum(1)
    delete_rows = (rowsq2 <= rowsq1 * thr )
    if sum(delete_rows) >0:
        out[delete_rows] = 0;
        rowsq2[delete_rows] = 1;
        print("zeroing %u rows" % sum(delete_rows))

    div_ = np.sqrt(rowsq2)
    out = (out.T / div_).T
    return out


if __name__ == "__main__":
    import sys
    import argparse
    parser = argparse.ArgumentParser('''plotter for QTL matrices [from binary hdf5 files]
    ''')
    parser.add_argument("covariates", type=str,
            help="covariates file")

    parser.add_argument("infile", type=str,
            help="input file")

    parser.add_argument("-o", "--outfile", type=str, default=None,
                        help="output file")

    parser.add_argument("-c", "--nindexcolumns", type=int, default=1,
                            help="number of index columns in the input")

    parser.add_argument("-b", "--chunksize", type=float, default= 1e5,
                            help= "number of lines to read at a time")

    args = parser.parse_args()
    ###############################################################################

    assert len(sys.argv)>2, "please provide TWO files! (1 - covariates, 2 - data )"    

    against_file = args.covariates
    input_file = args.infile

    nameparts = input_file.split('.')
    if nameparts[-1] in ["bed", "tab","txt","matrix"]:
        outname = '.'.join( nameparts[:-1] +  ["orth"] + [ nameparts[-1] ] )
    else:
        outname = input_file + ".orth"

    print( "see\t", outname, file = sys.stderr )

    adf = pd.read_table(input_file, index_col = np.arange(args.nindexcolumns) )
    bdf = pd.read_table(against_file, index_col = 0)

    print("shape of the    DATA    matrix: " , adf.shape )
    print("shape of the COVARIATES matrix: " , bdf.shape )

    common_cols = list(set(adf.columns) & set(bdf.columns))
    if len(common_cols) == len(adf.columns) and not len(common_cols) == len(bdf.columns):
        print("INFO: too many covariates columns", file = sys.stderr)
        bdf = bdf[adf.columns.tolist()]
    if not len(common_cols) == len(adf.columns):
        print("""WARNING: column sets do not match;
        not all data columns have matching covariates""", file = sys.stderr)
        common_cols = sorted(common_cols)
        adf = adf[common_cols]
        bdf = bdf[common_cols]

    if bdf.isnull().any().any():
        bdf = fix_covariates(bdf)
    #print("rows with zero variance:", (bdf.var(1) == 0).sum(), file = sys.stderr)
    #print(bdf[bdf.var(1) != 0].shape)
    adf = standardize(adf)
    if adf.isnull().any().any():
        print("NAs found in data after centering/scaling!")
    out = orthogonalize(adf, bdf)
    if out.isnull().any().any():
        print("NAs found!")
    out.to_csv( outname , sep= "\t" )
