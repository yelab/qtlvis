import numpy as np
import pandas as pd
from rpy2.robjects import FloatVector
from rpy2.robjects import Matrix
from rpy2 import robjects

from rpy2.robjects import pandas2ri, numpy2ri
numpy2ri.activate()
pandas2ri.activate()

rmatrix = robjects.r('as.matrix')

#####################################################################
# R package names
packnames = ('edgeR',)

# import rpy2's package module
import rpy2.robjects.packages as rpackages

if all(rpackages.isinstalled(x) for x in packnames):
    have_tutorial_packages = True
else:
    have_tutorial_packages = False
    if not have_tutorial_packages:
        # import R's utility package
        utils = rpackages.importr('utils')
        # select a mirror for R packages
        utils.chooseCRANmirror(ind=1) # select the first mirror in the list
#We are now ready to install packages using R's own function install.package:
if not have_tutorial_packages:
    # R vector of strings
    from rpy2.robjects.vectors import StrVector
    # file
    packnames_to_install = [x for x in packnames if not rpackages.isinstalled(x)]
    if len(packnames_to_install) > 0:
        utils.install_packages(StrVector(packnames_to_install))
        
DGEList = robjects.r("edgeR::DGEList")
calcNormFactors = robjects.r("edgeR::calcNormFactors")
calcNormFactorsTMM = lambda x: calcNormFactors(x , method="TMM")
_cpm_ = robjects.r("edgeR::cpm")

def cpm(xarray, **kwargs):
    kwargs = dict(zip( [ kk.replace("_", ".") for kk in kwargs.keys() ], kwargs.values() ) )

    #tmp = pd.np.array( _cpm_( xarray, **kwargs) )
    func = lambda xarray :  _cpm_( DGEList(xarray), **kwargs) 

    if type(xarray) is pd.DataFrame:
        cols = xarray.columns
        rows = xarray.index
        return pd.DataFrame( np.array(func( xarray.astype(float).as_matrix() )) , columns = cols, index = rows )
    else:
        return  np.array(func( xarray ))
    #return func( xarray )


def tmm(df, log = True, prior_count = 0.25, **kwargs):
    if type(df) == pd.DataFrame:
        dgelist = DGEList( df.as_matrix() )
    else:
        dgelist = DGEList( df )
    tmp = calcNormFactors( dgelist, method="TMM", **kwargs )
    #kwargs.update(

    kwargs = {"normalized.lib.sizes":True, "log" : log, "prior.count" : prior_count }
    tmp = pd.np.array( _cpm_( tmp, **kwargs) )
    assert tmp.shape == df.shape

    if type(df) == pd.DataFrame:
        return pd.DataFrame( tmp , index = df.index, columns= df.columns)
    else:
        return tmp

