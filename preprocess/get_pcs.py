#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from __future__ import print_function
import pandas as pd
import numpy as np
import sys, logging

def get_pcs(self, n_components = None, randomized = False, verbose = True,v = True):
    nrows = self.shape[0]
    if randomized or ( nrows > 1e8) :
        from sklearn.decomposition import RandomizedPCA as PCA
        logging.info("using randomized PCA")
    else:
        from sklearn.decomposition import PCA
        logging.info("using deterministic PCA")

    if not n_components:
        n_components = None

    "subtract mean"
    print("shape:", self.shape)
    print("types:", np.unique(self.dtypes) )
    self = (self.T - self.mean(axis = 1) ).T
    "drop singlular rows"
    self = self[ ~(self == 0 ).all(axis = 1) ]

    pca_ = PCA(n_components= n_components).fit(self)
    pca_.components_
    weights_ = pca_.transform(self)
    cols = [ 'PC%u' % (x+1) for x in range(pca_.components_.shape[0]) ]
    weights_df = pd.DataFrame(weights_, index = self.index, columns = cols)
    scores_df =  pd.DataFrame(pca_.components_.T, index = self.columns, columns = cols)
    if verbose == True & v == True :
        logging.info("PC\texplained variance, %\n" +
            "\n".join(map(lambda x: '%3u\t %.2f' % x, zip(range(1, 1+self.shape[1]), 100*pca_.explained_variance_ratio_))) )
    return weights_df, scores_df, pca_

###############################################################################
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser('''plotter for QTL matrices [from binary hdf5 files]
    ''')

    parser.add_argument("infile", type=str, 
            help="input file")

    parser.add_argument("-o", "--outfile", type=str, default=None,
                        help="output file")
                       
    parser.add_argument("-n", "--n_pcs", type=int, default=None,
                            help="number of PCs")

    parser.add_argument("-c", "--nindexcolumns", type=int, default=1,
                            help="number of index columns in the input")    

    parser.add_argument("-b", "--chunksize", type=float, default= 1e5,
                            help= "number of lines to read at a time")   

    parser.add_argument("-r", "--randomized", action='store_true',
                            help= "force randomized PCA") 

    parser.add_argument("-p", "--prefix", type = str, default = "",
                            help= "prefix to append to row names") 

    parser.add_argument("-s", "--suffix", type = str, default = "",
                            help= "suffix to append to row names") 
                            
    args = parser.parse_args()
    ###############################################################################
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    ###############################################################################
    args.chunksize = 5e5
    # args.infile = "../pergenegrouped/data/atac.raw.selected.agg.matrix.matched.normalized" 
    # sys.argv[1]  #"expressions.genes.0.matrix.eqtl.txt"
    suffix =  ("." if args.n_pcs is None else ".%u" % args.n_pcs ) + "pcs"
    outfile = args.infile +  suffix
    varexplfile = args.infile + ".varexpl" + suffix
    nrows = 1

    with open(args.infile, 'r') as f:
        ncol = len(f.readline().split("\t"))
        if not args.randomized:
            for _ in f:
                nrows += 1

    ite = pd.read_csv(args.infile, sep = '\t', index_col = False, # dtype=pd.np.float32,
                    usecols = np.arange(args.nindexcolumns, ncol), low_memory=False,
                    iterator = True,  na_filter=False, chunksize = args.chunksize)

    rna_df = pd.concat( list(ite), ignore_index=True).transpose()

    print(rna_df.shape)
    print(rna_df.head() , file=sys.stderr)

    print('individuals :', file=sys.stderr)
    print(*rna_df.index.tolist() , sep = "\t", file=sys.stderr)

    trnsf, _,  pca_ = get_pcs(rna_df, args.n_pcs, args.randomized, verbose = True)

    if args.suffix or args.prefix:
        trnsf.columns = [args.prefix + x + args.suffix for x in trnsf.columns]

    trnsf.transpose().to_csv( outfile , sep='\t')
    
    with open(varexplfile, "w+") as f:
        print("PC\texplained variance, %", file = f)
        print( *(map(lambda x: '%3u\t %.2f' % x, zip(range(1, 1+ rna_df.shape[1]), 100*pca_.explained_variance_ratio_))) ,
                sep = "\n", file = f)


