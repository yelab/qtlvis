#!/usr/bin/env python3
from __future__ import print_function
import sys
import pandas as pd
import numpy as np
from scipy.stats import rankdata
from orthogonalize import standardize

def quantile_normalization(X):
    """
    by Vincent Davis vincent at vincentdavis.net 
    anarray with samples in the columns and probes across the rows
    import numpy as np
    """
    if type(X) == pd.DataFrame:
        pdflag = True
        cols = X.columns
        rows = X.index

    X = np.array(X)
    XX = np.zeros_like(X)
    ii = np.argsort(X,axis=0)
    jj = np.arange(X.shape[1])

    XX[ii, jj] = np.mean( X[ ii, jj ], axis=1 )[:,np.newaxis]

    if pdflag:
        XX = pd.DataFrame(XX, index = rows, columns = cols )

    return XX


def my_quantile_normalization(xx):
    xxsort = xx.sort()

    xxmn = xx.mean(axis = 1) 
    xx = xx[xxmn>0]
    xxmn = xxmn[xxmn>0]

    rank = lambda x :  rankdata(x, method="ordinal")

    xxranks = xx.apply(rank, axis = 0).astype(int)
    xxranks.as_matrix()

    # sumranks = pd.DataFrame(rankdata(xx.sum(axis = 1)), index = xx.index)
    xmnrank = rank(xxmn)
    rankmean = pd.DataFrame(xxmn.tolist(), index = xmnrank )

    ranknormalized_df = pd.DataFrame(pd.np.squeeze(rankmean.as_matrix()[ xxranks -1]),
                 index = xxranks.index, columns= xxranks.columns )

    return ranknormalized_df

if __name__ == '__main__':
    infile = str(sys.argv[1])
    flaglog = True
    outfile = infile + (".log" if flaglog else "" )+ ".normalized"

    epsilon = 1/32

    xx = pd.read_csv( infile , sep = "\t")
    xx.set_index(xx.columns[0], inplace= True)

    print("columns: ", file = sys.stderr)
    [print(x, end = "\t", file = sys.stderr) for x in xx.columns]
    print("", file = sys.stderr)

    ranknormalized_df = quantile_normalization(xx)

    if flaglog:
        ranknormalized_df = ranknormalized_df.apply(lambda x : pd.np.log2(x + epsilon) )

    standartized = standardize(ranknormalized_df)
    # xxmn = ranknormalized_df.mean(axis = 1)
    # xxstd = ranknormalized_df.std(axis = 1)
    # standartized = ( (ranknormalized_df.T - xxmn) / xxstd ).T

    standartized.to_csv( outfile, sep = '\t')
