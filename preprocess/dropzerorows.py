
import pandas as pd
import sys

import argparse
parser = argparse.ArgumentParser('''plotter for QTL matrices [from binary hdf5 files]
''')
'/Users/dlituiev/repos/dnase_qtl_rna/freqhist/tabs/GM18486.0.counts.k15.pwm.tab'

parser.add_argument("infile", nargs='?', type=str,
        default= '/Users/dlituiev/repos/ataccounts/pwm_analysis/datapwm/IGTB1312.0.counts.gt4.k15.pwm.tab' ,
        help="output file (.csv); for stdout type '-' ")

parser.add_argument("-o", "--outfile", type=str, default=None,
                    help="output file")

parser.add_argument("-t", "--threshold", type=float, default= 0,
                        help= "minimal fraction of non-zeros")

args = parser.parse_args()
###############################################################################

infile = sys.argv[1]

outfile = infile + ".nz" if len(sys.argv)<3 else sys.argv[2]

df = pd.read_table( infile , index_col = 0)
valid = (df!=0).sum(axis = 1) > args.threshold  

if sum(valid) > 0:
    print( "dropping %u rows out of %u (%.2f %%)" % (len(valid) - sum(valid), len(valid),  100*( len(valid) - sum(valid) )/ len(valid)) , file = sys.stderr )
else:
    print( "preserving as is" , file = sys.stderr)

df = df[ valid ]

df.to_csv( outfile , sep = '\t' )

