#!/usr/bin/python
from __future__ import print_function
import sys
from runshell import runcmd
# print 'Number of arguments:', len(sys.argv), 'arguments.'
#####################################################
import argparse
parser = argparse.ArgumentParser('match [individual] columns in the supplied matrices')
parser.add_argument("infile_left", 
                    help="input file left")
#parser.add_argument("outFile", 
parser.add_argument("infile_right", 
                    help="input file right")
#                    help="output file (.csv); for stdout type '-' ")

parser.add_argument("-o", "--outdir", default ='',
                    help= "output directory")

parser.add_argument("-c", "--index_cols", default =1,
                    type=int, help="index columns in the second file")

parser.add_argument('-s', '--skip_first', action='store_true', help="suppress the first output")
parser.add_argument('-v', '--verbose', action='store_true', help="suppress the first output")
                    
args = parser.parse_args()
###############################################################################

"empty cells are skipped by awk if not set: FS = \t"

def printve(*x):
    if args.verbose: 
        print( x, file = sys.stderr )

infiles = [args.infile_left, args.infile_right]

from os import path

fbases = [None]*2
outdirs = [None]*2
for n in range(len(infiles)):
    outdirs[n], fbases[n] = path.split(infiles[n])
    # print( "output dir: ", outdirs[n], file = sys.stderr)
    print( "file base: ", fbases[n], file = sys.stderr)

if len(args.outdir):
    outdirs = [ args.outdir ]*2

outfiles = [None]*2
for n in [0,1]:
    outfiles[n] = path.join( outdirs[n] , fbases[n] + '.matched')

"read the headers"
samples = [[],[]]
for n in [0,1]:
    if infiles[n].endswith("bed"):
        n_index_cols = 3
    else:
        n_index_cols = 1
    with open(infiles[n]) as f:
        line = f.readline().rstrip('\n')
        samples[n] = line.split('\t')[n_index_cols:]
        printve(samples[n] )
        print('length of the sample %u: %u' % ( n, len(samples[n]) ) , file = sys.stderr )

inters = list( set(samples[0]) & set(samples[1]) )
if 'V1' in inters:
    inters.remove( 'V1')

inters.sort()

print('length of the intersection: %u' % len(inters) , file = sys.stderr)
printve('intersection:')
printve(inters)

if args.verbose or args.skip_first:
    notin1 = list( set(samples[0]) - set(samples[1]) )
    print('in #0 but not in #1: %u' % len(notin1) , file = sys.stderr)
    print(notin1 , file = sys.stderr)
    print("", file = sys.stderr)

if args.verbose:
    notin0 = list( set(samples[1]) - set(samples[0]) )
    print('in #1 but not in #0: %u' % len(notin0) , file = sys.stderr)
    print(notin0 , file = sys.stderr)
    print("", file = sys.stderr)


validlist = [[],[]]
for x in inters:
    for n in [0,1]:
        ix = samples[n].index(x)
        validlist[n].append( ix )
#        print('sample %u' % n, 'index %2u' % ix, 'value %s' % samples[n][ix], file = sys.stderr)
        ix = None

if args.verbose:
    for n in [0,1]:
        print('valid columns in sample %u ( %s ):' % (n, sys.argv[n+1]) , file = sys.stderr)
        print(validlist[n], file = sys.stderr)
        print('', file = sys.stderr)


assert ( len(validlist[0]) == len(validlist[1]) ), "list lengths do not match"

assert (len(validlist[0])>0), "empty list of individuals"

for n in [0,1]:
    if not n and args.skip_first:
        continue
    if infiles[n].endswith("bed"):
        index_cols = "$1,$2,$3"
    elif n==1 and args.index_cols!=1:
        index_cols = ", ".join(["$%u" % x for x in range(1, 1+args.index_cols)])
    else:
        index_cols = "$1"
    clmnlist = ','.join(['$%u' % (x + n_index_cols + 1) for x in validlist[n] ] )
    cmd = """awk 'BEGIN{ FS = "\\t"; OFS="\\t"} {print %(index_cols)s, %(clmns)s}' %(infile)s > %(outfile)s""" % \
        {"index_cols": index_cols, "clmns": clmnlist, "infile": infiles[n], "outfile": outfiles[n] }
    print("running command: [%u]" % (n+1), file = sys.stderr)
    print(cmd, file = sys.stderr)
    out = runcmd(cmd)
    if len(out[1])>0:
        print("returned[stderr]: ", out[1], file = sys.stderr)
 
