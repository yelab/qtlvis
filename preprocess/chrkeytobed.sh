awk 'BEGIN{FS="[-,:,\t]"; OFS = "\t" }  
($0 !~ /:/ ) &&(NR==1){ printf "#chrom\tstart\tend"; for (n=2; n<=NF; n++){ printf "\t%s", $n;} printf "\n" ; next}
($0  ~ /:/ ){printf "%s\t%u\t%u", $1, $2, $3; for (n=4; n<=NF; n++){ printf "\t%s", $n;} printf "\n" }' $1
