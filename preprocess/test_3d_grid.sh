#!/bin/bash

A=$1
B=$2
C=$3
N=$(( $((A * B)) *  C ))

AB=$(( A* B ))

echo -e "sample: $N" >&2
echo -e "A: $A\t B: $B\t C: $C" >&2
echo -e "=====================" >&2

for n in $(seq 0 $(($N - 1)) )
do
    res=$(( n % (AB) ))
    echo -e  "a:  $(( 1 + res %  $A ))  \t b: $(( 1 + res / A )) \t c: $(( 1 +  n / AB )) "
done

