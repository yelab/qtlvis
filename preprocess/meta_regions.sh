#!/bin/bash
echo -e "id\tchr\tposition"
cat $1 | awk -F'[:\t-]' '{print $1 ":" $2 "-" $3 "\t" $1 "\t" int(($2+$3)/2)}'
