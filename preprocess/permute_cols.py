#!/usr/bin/env python3
from __future__ import print_function
import sys
import numpy as np
from runshell import *
# print 'Number of arguments:', len(sys.argv), 'arguments.'

"empty cells are skipped by awk if not set: FS = \t"

###############################################################################
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("infile", 
                    help="input file (.vcf) with gene annotation")

parser.add_argument("outfile", nargs='?', type=str, default=r'',
                    help="output file; for stdout type '-' ")
                   
parser.add_argument("-v", "--verbose", action='store_true')

args = parser.parse_args()
###############################################################################
if  args.outfile == r'':
    outfile = args.infile+".permuted"
else:
    outfile = args.outfile
###############################################################################
def vprint(*argvs, **kwargs):
    if args.verbose:
        print(argvs, kwargs, file = sys.stderr)

"read the headers"
with open(args.infile) as f:
    line = f.readline().rstrip('\n')
    samples = line.split('\t')[1:]
    print('number of individuals: %u' % ( len(samples) ) , file = sys.stderr )

vprint('individual labels before:')
vprint(samples)

N = len(samples)

clmn_num = np.arange(0,N)
np.random.shuffle(clmn_num)

clmnlist = ','.join(['$%u' % (x + 2) for x in clmn_num] )
cmd = """head -n1 %(infile)s > %(outfile)s;
awk 'BEGIN{ FS = "\\t"; OFS="\\t"} NR>1{print $1,%(clmns)s}' %(infile)s >> %(outfile)s""" % \
    {"clmns": clmnlist, "infile": args.infile, "outfile": outfile }
vprint("running command:", file = sys.stderr)
vprint(cmd, file = sys.stderr)
out = runcmd(cmd)
if len(out[1]):
    print("returned[stderr]: ", out[1], file = sys.stderr)

print("col-permuted matrix written to : %s" % outfile, file = sys.stderr)


