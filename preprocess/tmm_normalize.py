#!/usr/bin/env python3
from __future__ import print_function
import sys, logging
import numpy as np
import pandas as pd
import math

"""
translated from edgeR package by Robinson and Oshlack, 2010
"""

def calcFactorWeighted(obs, ref, libsize_obs=None, libsize_ref=None, logratioTrim=.3,
                       sumTrim=0.05, doWeighting=True, Acutoff=-1e10):
    log2 = np.log2
    if all(obs==ref):
        return 1

    if libsize_ref is None:
        nR = math.fsum(ref)
    else:
        nR  = libsize_ref
    
    if libsize_obs is None:
        nO = math.fsum(obs) 
    else:
        nO = libsize_obs
    
    if nO == 0:
        return 1

    logR = log2(obs) - log2(nO)- log2(ref)  + log2(nR)       # log ratio of expression, accounting for library size
    # logR = log2((obs/nO)/(ref/nR))         # log ratio of expression, accounting for library size
    absE = (log2(obs/nO) + log2(ref/nR))/2 # absolute expression
    assympt_var = (nO-obs)/nO/obs + (nR-ref)/nR/ref   # estimated asymptotic variance

    #   remove infinite values, cutoff based on A
    fin = ~np.isinf(logR) & ~np.isinf(absE) & (absE > Acutoff)
    
    #print("valid #:", sum(fin))
    if sum(fin) ==0:
        return 1
    
    logR = logR[fin]
    absE = absE[fin]
    assympt_var = assympt_var[fin]

    #   taken from the original mean() function
    n = len(logR)

    loL = np.floor(n * logratioTrim) 
    hiL = n  - loL
    loS = np.floor(n * sumTrim)
    hiS = n  - loS

    #   keep <- (rank(logR) %in% loL:hiL) & (rank(absE) %in% loS:hiS)
    #   a fix from leonardo ivan almonacid cardenas, since rank() can return
    #   non-integer values when there are a lot of ties
    from scipy.stats import rankdata
    rrank = lambda x: np.array(rankdata(x))
    
    keep = ( (rrank(logR)>=loL) & (rrank(logR)<=hiL) ) & \
           ( (rrank(absE)>=loS) & (rrank(absE)<=hiS) )
        
    if (doWeighting):        
        logf = math.fsum(logR[keep]/assympt_var[keep]) / math.fsum(1/assympt_var[keep])
    else:
        logf = mean(logR[keep])

    #   Results will be missing if the two libraries share no features with positive counts
    #   In this case, return unity
    if np.isnan(logf):
        logf = 0
    
    f = 2**logf
    return f

def get_ref_col( x, p = 0.75):
    f75 = calcFactorQuantile(x, p=0.75)
    refcol = np.argmin(abs(f75- np.mean(f75)))
    return refcol

def calcFactorQuantile(data, p):
    lib_size = data.sum(axis = 0)
    y = (data/lib_size)
    if type(data) == pd.DataFrame:
        y.iloc[:, np.array(np.logical_not(lib_size))] = 0
    else:
        y[:,np.logical_not(lib_size)] = 0
    f = np.percentile(y, 100*p, axis = 0)
    return f
    
def calcNormFactorsTMM(x, refcol = None,
        logratioTrim=.3, sumTrim=0.05, doWeighting=True, Acutoff=-1e10 ):
    lib_size = x.sum(axis = 0).astype(float)
    
    #Check method
    #method = match.arg(method)

    #Remove all zero rows
    allzero = (x.sum(1)>0) == 0
    if any(allzero):
        x = x[~allzero]
    logging.info( repr(x.shape) )

    #Degenerate cases
        #if(x.shape[0]==0  or x.shape[1] ==1):
        #   method="none"

    #Calculate factors
    if refcol is None:
        f75 = calcFactorQuantile(x, p=0.75)
        #print("f75", *f75, sep = "\n")
        refcol = np.argmin(abs(f75- np.mean(f75)))
    
    if type(x) == pd.DataFrame and type(refcol) is str:
        refcol = x.columns.tolist().index(refcol)

    if ( refcol < 0 | refcol > x.shape[1]) :
        logging.warining( "strange reference column specified:\t%s" % refcol )
        refcol = 0

        
    if type(x) == pd.DataFrame:
        logging.info( "reference column:\t%u\t%s" % (refcol, x.columns[refcol]) )
        ref = x.iloc[:,refcol]
        f = x.apply(lambda x: calcFactorWeighted(np.array(x),
                                ref= ref, logratioTrim=logratioTrim,
                                sumTrim=sumTrim, doWeighting=doWeighting, Acutoff=Acutoff),
               axis = 0)
    else:
        f = np.nan * np.ones(x.shape[1])
        ref = x[:,refcol]
        for i in range(x.shape[1]):
            f[i] = calcFactorWeighted(obs=x[:,i], ref= ref, logratioTrim=logratioTrim,
                                      sumTrim=sumTrim, doWeighting=doWeighting, Acutoff=Acutoff)
    #   Factors should multiply to one
    f = f/np.exp(np.mean(np.log(f)))
    logging.debug("norm_factors:\n%s" % repr(  f  ) )
    return (f, lib_size)

def calcNormFactorsTMM_safe(infile, index_col = None, ncol = 1, refcol = None,
                logratioTrim=.3, sumTrim=0.05, doWeighting=True, Acutoff=-1e10,
                raw = False, minnon0frac = 0.0):

    df = pd.read_table( infile, index_col=index_col, nrows=int(1e6/ncol) ).fillna(0)
    cols = df.columns.tolist()

    logging.debug( "index columns:\t%s" % list(df.index.names)  )
    if refcol is None:
        refcolnum = get_ref_col( df, p = 0.75)
        refcolname = cols[refcolnum]
    else:
        if type(refcol) is int and refcol not in df.columns.tolist():
            refcolname = cols[refcol]
        else:
            refcolname = refcol

    logging.info( "reference column:\t%u\t%s" % (refcolnum,  refcolname ) )
    #refcolser = pd.read_table( infile, index_col = None, usecols =  [refcolname] ).iloc[:,0].fillna(0)
    refcolser = pd.read_table(infile, index_col = df.index.names, 
                              usecols = df.index.names + [refcolname],
                              squeeze=True ).fillna(0)

    allzeros = (refcolser == 0).as_matrix().astype(int)

    for cc in cols:
        if cc == refcolname:
            continue
        #othcol =  pd.read_table(infile , index_col = None, usecols = [cc]).\
        othcol =  pd.read_table(infile , index_col = df.index.names, 
                                usecols = df.index.names + [cc],
                                squeeze=True ).fillna(0)
        logging.debug("col: %s\tnrows = %u" % (cc, othcol.shape[0] ) )
        allzeros += (othcol == 0).as_matrix()
    
    allzeros = allzeros >= len(cols)*( 1 - minnon0frac )

    assert allzeros.sum() < len(allzeros), "all rows marked as mostly zero rows"
    nflist = []
    for cc in cols:
        if cc == refcolname:
            nflist.append( (refcolname, 1, refcolser.sum() ) )
            continue
        #othcol =  pd.read_table( args.infile , index_col = None, usecols = [cc]).\
        #              iloc[:,0].fillna(0).astype('float64')
        othcol =  pd.read_table(infile, index_col = df.index.names,
                                usecols = df.index.names + [cc],
                                squeeze=True).fillna(0)
        logging.debug("col: %s\tnrows = %u" % (cc, othcol.shape[0] ) )

        col_lib_size = othcol.sum()
        df =  pd.concat([refcolser[~allzeros], othcol[~allzeros] ], axis = 1)
        norm_fact = calcFactorWeighted(np.array(othcol[~allzeros]),
                                    ref= np.array(refcolser[~allzeros]),
                                    logratioTrim=logratioTrim, sumTrim=sumTrim,
                                    doWeighting=doWeighting, Acutoff=Acutoff)
        #norm_fact = calcNormFactorsTMM( df , refcol = 0, raw = True)
        del othcol
        logging.debug("column\t%s\tfactor:\t%f" % (cc , norm_fact ))
        nflist.append( (cc, norm_fact, col_lib_size ) )

        #coldict[cc] = tmm(othcol, norm_fact[-1])
    #dfnorm = pd.concat( coldict , axis = 1)

    nfser = pd.DataFrame( nflist, columns = ["sample", "norm_factor", "lib_size",]).\
                set_index("sample")
    nfser["norm_factor"] = nfser["norm_factor"] /\
                               np.exp(np.mean(np.log(nfser["norm_factor"])))

    logging.debug("norm_factors:\n%s" % repr(  nfser["norm_factor"] ) )
    return ( nfser["norm_factor"], nfser["lib_size"] )


def get_weighted_lib_size(x, refcol = None, normalize_lib_size = True,
                    norm_fact = None,  index_col = None, ncol = 1):

    if normalize_lib_size:
        if not norm_fact:
            if type(x) is not str:
                (norm_fact, lib_size) = calcNormFactorsTMM(x, refcol = refcol)
            else:
                (norm_fact, lib_size) = calcNormFactorsTMM_safe(x, refcol = refcol,
                                                   index_col = index_col, ncol = 1,) 
        lib_size = lib_size * norm_fact
    else:
        lib_size = x.sum(axis = 0).astype(float)

    return lib_size


def normalize(x, lib_size, cpm = True, prior_count = .25, log = False):
    if log:
        prior_count_scaled = lib_size / np.mean(lib_size) * prior_count
        lib_size += 2 * prior_count_scaled

    if cpm:
        lib_size *= 1e-6
 
    eps = 1e-16
    assert all(lib_size) > eps, "too small lib_size values\n%s" % \
                                 repr(lib_size[lib_size <= eps])

    if log:
        return np.log2( ((x + prior_count_scaled)/ lib_size ) )
    else:
        return x/lib_size


def tmm(x, log = False, prior_count = .25, cpm = True,
            refcol = None, normalize_lib_size = True,
            norm_fact = None,  index_col = None, ncol = 1,):

    lib_size = get_weighted_lib_size( x,  refcol = refcol,  index_col = index_col,
                    ncol = ncol,
                    normalize_lib_size = normalize_lib_size,
                    norm_fact = norm_fact,)

    logging.debug("[tmm] weighted library size:\n%s" % repr(lib_size) )
       
    return normalize(x, lib_size, cpm = cpm, prior_count = prior_count, log = log)

  
from subprocess import check_output

def wc(filename):
    return int(check_output(["wc", "-l", filename]).split()[0])
##################################################################
if __name__ == "__main__":

    #####################################################
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", 
                        help="input file (.tab-separated ) with gene annotation")

    parser.add_argument("outFile", nargs='?', type=str, default=r'',
                        help="output file (.csv); for stdout type '-' ")
                        
    parser.add_argument("-c", "--n_ind_cols", default=1, type = int,
                        help = "number of index columns" )

    parser.add_argument("-r", "--refcol", type=str, default='',)

    parser.add_argument('-d', "--drop_all_zero_rows", action='store_true',
                         default=False)

    parser.add_argument('-l', "--log", action='store_true', dest = "log", default=True)
    parser.add_argument('-L', "--lin", action='store_false', dest = "log", 
                        default=True)

    parser.add_argument('-S', "--safe", action='store_true', dest = "safe", 
                        default=False)

    args = parser.parse_args()
    #####################################################
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    index_col = list(range(args.n_ind_cols))

    nffile = args.infile + ".norm_factors.tab"
    outfile = args.infile + (".log" if args.log else "") +  ".tmm"

    df = pd.read_table( args.infile , index_col = index_col, nrows = 3)
    if args.drop_all_zero_rows:
        df.dropna( axis = 0, inplace = True )

    cols = df.columns.tolist()
    indexcols = list(df.index.names)
    ncol = len(cols)
    nrow = wc(args.infile)

    size = nrow * ncol

    refcol = args.refcol if len(args.refcol)>0 else None
    if size < 1e8 and (not args.safe):
        logging.info( "regular in-memory normalization")
        df = pd.read_table( args.infile , index_col = index_col).fillna(0)
        logging.debug( "index columns:\t%s" % list(df.index.names) ,)
        dfnorm = tmm(df, log = args.log, refcol = refcol)
        dfnorm.to_csv( outfile , sep = "\t")
    else:
        logging.info( "memory-safe normalization")
        logging.debug( "table size\t%u" % size)
        logging.debug( "args.safe\t%s" % args.safe)
        lib_size  = get_weighted_lib_size(args.infile, index_col = index_col,
                                          ncol = ncol, refcol = refcol)
        lib_size.to_csv(nffile, sep = "\t")
        logging.info( "see normalization weights\t%s" % nffile,)
        """ normalize factors so that they multiply to one """

        #logging.debug( "nfser:\t%s" % repr(nfser.head()) )

        """read-write row-wise"""
        chunksize = int(1e4)
        dfiter = pd.read_table(args.infile, index_col = index_col,
                               chunksize = chunksize)

        with open( outfile, "w+" ) as outf:

            for nn , chunk in enumerate(dfiter):
                chunk = normalize( chunk.fillna(0) , lib_size , log = args.log )

                assert not chunk.isnull().any().any(), "NaNs in table chunk %s" % \
                                         repr( chunk[chunk.isnull().any(1)].head() )

                logging.debug( "chunk\t%s" % repr(chunk.head()) )
                chunk.to_csv( outf, sep = "\t", header = (nn ==0) )
                logging.debug("chunk\t%u\trows\t%u" % (nn, chunksize * (1+nn) ) )
             
    logging.info( "see", outfile, sep = "\t")
